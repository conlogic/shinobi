..
 =============================================================================
 Title          : Simple build system based on Ninja - aka Shinobi

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-03-23

 Description    : Change log for the package.
 =============================================================================


.. include:: ../CHANGELOG.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi
