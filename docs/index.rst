..
 =============================================================================
 Title          : Simple build system based on Ninja - aka Shinobi

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-03-23

 Description    : Master file for the documentation.
 =============================================================================


==============================================================================
Documentation for ``shinobi``
==============================================================================

.. toctree::
   :maxdepth: 2

   overview
   usage


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi
