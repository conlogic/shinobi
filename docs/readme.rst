..
 =============================================================================
 Title          : Simple build system based on Ninja - aka Shinobi

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-10-18

 Description    : Read me file for the package.
 =============================================================================


.. include:: ../README.rst


A simple *Shinobi* configuration file
=====================================

The features of *Shinobi* should be illustrated here using a simple example.
It comprises a tiny C implementation to calculate Fibonacci numbers.  Its
source code is located below the repository subdirectory
``examples/fibonacci/src``, and it is given by the following files:

+ the main program:
  :download:`fibonacci.c <../examples/fibonacci/src/c/fibonacci.c>`,

+ the real Fibonacci number calculation:
  :download:`calc_fib.c <../examples/fibonacci/src/c/calc_fib.c>`
  with the corresponding header file:
  :download:`calc_fib.h <../examples/fibonacci/src/include/calc_fib.h>`

To build this program a GCC C compiler is used that supports the C99 language
standard.  Building will produce a single ``fibonacci`` executable.

A *Shinobi* configuration file in the CONF format to build ``fibonacci`` could
look like:

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini

As mentioned above, as feature 1a, *Shinobi* can use configuration files both
in the CONF format just given, and in JSON format.  Here is the JSON file that
defines the same *Shinobi* configuration:

.. literalinclude:: ../examples/fibonacci/shinobi_rel.json
   :language: ini

Whereas such a JSON file seems to show the overall structure of sections and
parameters more clearly, a CONF file can be made more readable by using
comments.  Therefore we will highlight some key features of *Shinobi* using
the *Shinobi* configuration file in CONF format.  But before starting with
this let us note that the CONF format used is enhanced to use (flat) list
values in a syntax similar to JSON.

First of all, there some sections and parameters have fixed meanings.  For
instance, the section ``vars`` defines the global Ninja variables to be used:

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini
   :lines: 12-16
   :emphasize-lines: 1

Next, section ``rules`` specifies all Ninja rules:

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini
   :lines: 18-22
   :emphasize-lines: 1

Every section ``goal.<something>`` defines a Shinobi goal named ``<something>``
where '<something>' is only a placeholder for the real goal name.  Our example
has two goals sections: ``goal.cc`` and ``goal.lnk``:

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini
   :lines: 24-34
   :emphasize-lines: 1,7

The first one, `goal.cc`` is a multi goal specific for *Shinobi* mentions as
feature 4 above. Such a multi goal defines a bunch of Ninja goals that have
the same structure - ``goal.cc`` actually let us compile every of the two C
implementation files ``fibonacci.c`` and ``calc_fib.c`` into a corresponding
object file using rule ``cc``.  The other goal section ``goal.lnk`` form the
example is nothing special: it specifies a single Ninja goal that links, via
rule ``lnk`` the object files into an executable.

As mentioned in the feature 1 above, one can refer within parameter values to
other values is used a lot with our example configuration.  For instance, the
value ``%{paths.bindir}%/%{paths.exe}%`` for parameter ``out`` in section
``goals.link`` is an example for feature 1b: it refers, by its part
``%{paths.bindir}%`` to the value of parameter ``bindir`` in section
``paths``, and by ``%{paths.exe}%`` to the value of parameter ``exe`` in the
same section.

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini
   :lines: 30-50
   :emphasize-lines: 5,17,21

The other way to refer to another value within a parameter value mentioned
as feature 1c above is by calling a function.  The can be seen, for instance,
in our example in the value ``%(find_ext_in_dir c, %{paths.srcdir}%)%`` for
parameter ``srcs`` in section ``paths``:  to get this value, function
``find_ext_in_dir`` is called for two arguments ``c`` and the expanded value
of ``%{paths.srcdir}%``, i.e. the value of parameter ``srcdir`` in the same
section ``paths``.  Actually, function ``find_ext_in_dir`` finds the paths for
all ``*.c`` files in the directory whose path is given by the 2nd argument.

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini
   :lines: 36-40
   :emphasize-lines: 3,5

For feature 2 from above, i.e. to set environment variables for the  Ninja
call, section ``env.vars`` is used.  In our example, exactly one variable
``CPATH`` is set to expanded value of ``%(join_paths %{paths.headerdirs}%)%``.

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini
   :lines: 4-6
   :emphasize-lines: 1

By the way, this is, again, a function call reference: function ``join_paths``
is called to concatenate all elements in directory paths list
``%{paths.headerdirs}%`` given by parameter ``headerdirs`` in section
``paths``.

As mentioned above as feature 3, the command to call Ninja can itself be
configured by using parameter ``ninja_cmd`` in section ``env``.  In our
example that is only hinted by ``<ninja_cmd>`` using a placeholder for the
real ninja command to be used:

.. literalinclude:: ../examples/fibonacci/shinobi_rel.conf
   :language: ini
   :lines: 8-10
   :emphasize-lines: 3

To give a more concrete example how to use the ``ninja_cmd``: Let us suppose
that we have a script ``setenv.sh`` that we want to source before really
calling Ninja for the created recipe.  In this case we could use:

.. code-block:: ini

   [env]
   # Ninja command to be used.
   ninja_cmd = setenv.sh && ninja

By the way, section ``paths`` has, other than the other sections mentioned, no
fixed meaning in *Shinobi* configuration files.  In our example this section
is used to define a bunch of path and name parameters in a convenient and
modular way.

After the description of our *Shinobi* example configuration file(s) let us
have a short look on the Ninja recipe file generated by *Shinobi* on the fly
from it:

.. literalinclude:: ../examples/fibonacci/build_rel.ninja

Please note that the original *Shinobi* multi goal ``cc`` is expanded into two
Nina goals ("builds" in Ninja terminology) - one for each C implementation
file.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi
