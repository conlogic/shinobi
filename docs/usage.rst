..
 =============================================================================
 Title          : Simple build system based on Ninja - aka Shinobi

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-10-22

 Description    : Description of the user interface.
 =============================================================================


==================================
How to use and configure *Shinobi*
==================================


Run *Shinobi*
=============


The commandline interface of *Shinobi* is ``snob``.  By default, either a
Shinobi configuration file ``shinobi.conf`` in CONF format or ``shinobi.json``
in JSON format is expected in the base directory, whereas ``shinobi.conf`` is
preferred if both files are available.

Beside this, ``snob`` has various options.  These are:

+ standard option ``-h`` / ``--help`` to print the help message.

+ another standard option ``-V`` / ``--version`` to print the version of
  *Shinobi*.

+ Option ``-b`` / ``--base-dir`` to set the path for an alternative base
  directory instead of the current working directory.

+ Option ``-c`` / ``--config-file`` to set the path for an alternative Shinobi
  configuration file.  Please note that a file name and a relative path as
  parameter is taken relative to the used base directory that can be different
  from the current working directory when using ``-b`` / ``--base-dir``.

+ Options to control the log messages emitted when running *Shinobi*: ``-v`` /
  ``--verbose`` to increase verbosity, and ``-d`` / ``--debug`` to enable all
  log messages including those for debugging.

+ Options to control running of *Shinobi* itself: ``-n`` / ``--dry-run`` to do
  a simulation instead a real run, ``-k`` / ``--keep-going`` to keep running
  even when errors occur, and ``-j`` / ``--nr-tasks`` to control the number of
  parallel tasks used by Ninja.

For details, use ``snob -h`` or, equivalently, ``snob --help``.


Configure *Shinobi*
===================


-----------------
A running example
-----------------

To illustrate the coming reference for the content of *Shinobi* configuration
files we use a running example.


The example source code
-----------------------

This running example implements a simple number guessing game for a single
player in C.  You start the program with a commandline argument.  It specifies
the upper bound for the range of natural numbers the guessed number is
secretly chosen from.  Then the player has to guess this secret number.  For
wrong guesses of the secret number the player gets an hint whether the guess
was to large or to small.

Let us have a quick look at the code.  First we have the code for the game
framework in directory with subdirectory ``game``.  The game framework has a C
header file ``game/guess.h`` that declares its functions:

.. literalinclude:: ../examples/guess_nat/src/game/guess.h
   :language: c

Beside this, the game framework consists of two C implementation files -
``game/guess.c`` with the game's main implementation, and ``games/secret.c``
to get the secretly chosen number to be guessed:

.. literalinclude:: ../examples/guess_nat/src/game/guess.c
   :language: c

.. literalinclude:: ../examples/guess_nat/src/game/secret.c
   :language: c

The code for the players reside below subdirectory ``players``.  The interface
for a player is specified in C header file ``players/player.h``:

.. literalinclude:: ../examples/guess_nat/src/players/player.h
   :language: c

We have two types of players.  First, file ``players/interact/player.c``
implements an interactive player:

.. literalinclude:: ../examples/guess_nat/src/players/interact/player.c
   :language: c

Second, file ``players/computer/player.c`` implements a simple computer player
(using iteration by halving the known interval):

.. literalinclude:: ../examples/guess_nat/src/players/computer/player.c
   :language: c

The example *Shinobi* configuration files
-----------------------------------------

To build both program variants a single *Shinobi* configuration file is
provided - both in CONF format, and explained by many comments:

.. literalinclude:: ../examples/guess_nat/shinobi.conf
   :language: ini

and in JSON format:

.. literalinclude:: ../examples/guess_nat/shinobi.json
   :language: json

Please note that the value ``<basedir>`` for the parameter with base name
``srcdir`` in section ``paths`` is only a placeholder for a real value.


The generated Ninja build file for the example
-----------------------------------------------

Just for comparison, here are the Ninja recipe file generated by *Shinobi* on
the fly from the *Shinobi* configuration file of each format for the example.
Please note that the value ``<basedir>``, like in the *Shinobi* configuration
files for this example, is only a placeholder for concrete path of the base
directory.

.. literalinclude:: ../examples/guess_nat/build.ninja


-------------------------------------------
Reference for *Shinobi* configuration files
-------------------------------------------

Let us given a detailed description for the syntax of *Shinobi* configuration
files.  The *Shinobi* configuration files for the running example just given
will be used to illustrate these descriptions.


Structure of *Shinobi* configuration files
------------------------------------------

+ Representation of parameters:

  Parameters are built of its name ``<name>`` and an associated value
  ``<value>``.  The full name ``<name>`` of a parameter always consists of a
  section ``<section>`` and a base name part ``<base>``, where the full
  ``<name>`` is represented as ``<section>.<base>``.  Section part
  ``<section>`` can be itself consist of multiple parts.  In the parameter
  name representation such parts are also separated by a ``.``.

+ Realization of parameters:

  The concrete realization of a parameter depends on the configuration file
  format.  Let us consider a simple section parameter with full name
  ``<section>.<base>``; and a multipart section parameter with full name
  ``<sect1>.<sect2>.<base>``, i.e. whose section part ``<section>`` is
  ``<sect1>.<sect2>`` - meaning it consists of the two parts ``<sect1>`` and
  ``<sect2>``.  Both parameters are supposed to have value ``<value>``.  Then
  we have:

  - CONF format: the section part ``<section>`` becomes a section, the
    ``<base>`` part turns into an option:

    .. code-block:: ini

       [<section>]
       <base> = <value>

    An example: parameter with full name ``vars.ccopts``:

    .. literalinclude:: ../examples/guess_nat/shinobi.conf
       :language: ini
       :lines: 8-10
       :emphasize-lines: 1,3

    There are no special treatment of multipart sections - the full section
    is always used as section name:

    .. code-block:: ini

       [<sect1>.<sect2>]
       <base> = <value>

    An example: parameter with full name ``env.vars.CPATH``:

    .. literalinclude:: ../examples/guess_nat/shinobi.conf
       :language: ini
       :lines: 4-6
       :emphasize-lines: 1,3

  - JSON format: the section part ``<section>`` is a top-level key, and
    ``<base>`` becomes a key within the dictionary associated with key
    ``<section>``:

    .. code-block:: json

       {
         "<section>": {
           "<base>": "<value>"
         }
       }

    Please not that a ``,`` (i.e. a comma) is needed in JSON at the end of an
    item if this item is not the last one within a dictionary.

    An example: parameter with name full ``vars.ccopts``:

    .. literalinclude:: ../examples/guess_nat/shinobi.json
       :language: json
       :lines: 1,7-11,58
       :emphasize-lines: 1-3,5,7

    For multipart sections an additional dictionary nesting level is used for
    every section part:

    .. code-block:: json

       {
        "<sect1>": {
         "<sect2>": {
          "<base>": "<value>"
         }
        }
       }

    An example: parameter with full name ``env.vars.CPATH``:

    .. literalinclude:: ../examples/guess_nat/shinobi.json
       :language: json
       :lines: 1-7,58
       :emphasize-lines: 1,2-5,6,8

+ Types of parameter values:

  All parameter values are either scalar (string) values or lists of such
  values.  Scalar values are represented directly by giving their literal
  value irrespective of the configuration file format.  For list values the
  two formats use a similar syntax.  But there is subtle requirement if
  one use multiple lines for a list value in a CONF file.

  - CONF files: use ``[`` / ``]`` to mark the begin / end of a list, ``,`` to
    separate elements.  Multiple lines can be used, but then all but the first
    line must be indented by the same amount.

    An example:

    .. literalinclude:: ../examples/guess_nat/shinobi.conf
       :language: ini
       :lines: 45-64
       :emphasize-lines: 14-17

    This defines parameter with full name ``paths.headerdirs`` whose value is
    the list with the two element ``"%{paths.srcdir_game}%`` and
    ``%{paths.srcdir_players}%``.

  - JSON files: here the normal JSON list notation is used.

    An example:

    .. literalinclude:: ../examples/guess_nat/shinobi.json
       :language: json
       :lines: 1,32-42,57,58
       :emphasize-lines: 8-11

    Here the same parameter is defined as in the CONF example above.

+ Reusing other values in parameter values:

  There are two ways to reuse other values in parameter values: we can either
  refer to the value of another parameter, or we can use the value of a
  function call using a function available for configuration.

  - Referring to the value of another parameter:

    If we want to use the value of another parameter with full name
    ``<refname>`` within the value ``<value>`` of a parameter, we use
    ``%{<refname>}%`` within ``<value>``.  Please note that both CONF and JSON
    configuration files use the same syntax.

    A CONF example:

    .. literalinclude:: ../examples/guess_nat/shinobi.conf
       :language: ini
       :lines: 45-51
       :emphasize-lines: 3,6

    and the corresponding JSON example:

    .. literalinclude:: ../examples/guess_nat/shinobi.json
       :language: json
       :lines: 1,32-35,57,58
       :emphasize-lines: 3,4

    In both cases the value of parameter with full name ``paths.srcdir_game``
    refers to the value of the parameter with full name ``paths.srcdir``
    (defined, in both cases, in the other highlighted line).


  - Using the value of function calls:

    Let us consider a function with name ``<func>`` available for
    configuration files. If we want to use the value of the ``<func>``
    function called with one argument ``<arg>`` within the value ``<value>``
    of a parameter, we use ``%(<func> <arg>)%`` within ``<value>``.  If the
    ``<func>`` function should be called with more arguments ``<arg1>``,
    ``<arg2>``, ..., we use ``%(<func> <arg1>, <arg2>...)%`` within
    ``<value>`` instead.  Such function calls within ``<value>`` can also be
    nested.  Please note that, again, both CONF and JSON configuration files
    use the same syntax.

    A CONF example for a one-argument function call:

    .. literalinclude:: ../examples/guess_nat/shinobi.conf
       :language: ini
       :lines: 4-7
       :emphasize-lines: 3

    and the corresponding JSON example:

    .. literalinclude:: ../examples/guess_nat/shinobi.json
       :language: json
       :lines: 1-7,58
       :emphasize-lines: 4

    In both cases function :py:func:`join_paths` is called for argument
    ``%{paths.headerdirs}%``.

    A CONF example for a two-argument function call:

    .. literalinclude:: ../examples/guess_nat/shinobi.conf
       :language: ini
       :lines: 45-75
       :emphasize-lines: 31

    and the corresponding JSON example:

    .. literalinclude:: ../examples/guess_nat/shinobi.json
       :language: json
       :lines: 1,32-50,57,58
       :emphasize-lines: 20

    Here function :py:func:`find_ext_in_dirs` is called for arguments ``c``
    and ``%{paths.srcdirs_man}%``.

    Finally a CONF example for a nested function calls:

    .. literalinclude:: ../examples/guess_nat/shinobi.conf
       :language: ini
       :lines: 45-83
       :emphasize-lines: 39

    and the corresponding JSON example:

    .. literalinclude:: ../examples/guess_nat/shinobi.json
       :language: json
       :lines: 1,32-53,57,58
       :emphasize-lines: 23

    Here first function :py:func:`files_noext` is applied to argument
    ``%{paths.srcs_man}%``, and then, for value of this function call and
    ``o``, function :py:func:`add_files_ext` is called.

    As already mentioned you cannot call arbitrary functions within parameter
    values but only those that are available for configuration files.  Here is
    a complete list of them:

    .. autofunction:: shinobi.functions.file_base

    .. autofunction:: shinobi.functions.files_bases

    .. autofunction:: shinobi.functions.file_dir

    .. autofunction:: shinobi.functions.files_dirs

    .. autofunction:: shinobi.functions.file_noext

    .. autofunction:: shinobi.functions.files_noext

    .. autofunction:: shinobi.functions.add_file_ext

    .. autofunction:: shinobi.functions.add_files_ext

    .. autofunction:: shinobi.functions.add_file_dir

    .. autofunction:: shinobi.functions.add_files_dir

    .. autofunction:: shinobi.functions.join_paths

    .. autofunction:: shinobi.functions.join_lists

    .. autofunction:: shinobi.functions.diff_lists

    .. autofunction:: shinobi.functions.common_lists

    .. autofunction:: shinobi.functions.unique_list

    .. autofunction:: shinobi.functions.find_ext_in_dir

    .. autofunction:: shinobi.functions.find_ext_in_dirs


Sections and parameters with fixed meaning in configuration files
-----------------------------------------------------------------

Here we describe all sections and parameters for *Shinobi* configuration files
with a fixed meaning.  Parameters are grouped by their sections and listed by
their base names.  Furthermore for every described section or parameter is
mentioned whether its is mandatory for *Shinobi* configuration files, or
optional.  Because CONF files are IMHO easier to read we confine to give
examples from the *Shinobi* configuration file of our running example in the
CONF format.

+ Section ``env`` (optional):

  - Parameter ``ninja_cmd`` (optional):

    The Ninja command to be used is specified.

    Example:

    Actually, this parameter is not used in our running example.  One use of
    it would be to use a ``ninja`` command at a non-standard path, say
    ``/path/to/ninja``, by putting into the Shinobi CONF configuration file:

    .. code-block:: ini

       [env]
       ninja_cmd = /path/to/ninja

+ Section ``env.vars`` (optional):

  Every parameter ``<var>`` defines environment variable ``<var>`` and set
  their value to the parameter value during the Ninja call.

  Example:

  .. literalinclude:: ../examples/guess_nat/shinobi.conf
     :language: ini
     :lines: 3-6
     :emphasize-lines: 1,3

  Environment variable ``CPATH`` is set to the value resulting from expanding
  ``%(join_paths %{paths.headerdirs}%)%``.

+ Section ``vars`` (optional):

  Every parameter ``<var>`` defines global Ninja variable ``<var>`` and set
  their value to the parameter value.

  Example:

  .. literalinclude:: ../examples/guess_nat/shinobi.conf
     :language: ini
     :lines: 8-10
     :emphasize-lines: 1,3

  Global Ninja variable ``ccopts`` with value ``-c -std=c99 -Wall -pedantic``
  is defined.

+ Section ``rules`` (mandatory):

  Every parameter ``<rule>`` defines a Ninja rule with name ``<rule>`` whose
  command is the parameter value.

  Example:

  .. literalinclude:: ../examples/guess_nat/shinobi.conf
     :language: ini
     :lines: 13-16
     :emphasize-lines: 1,3

  Ninja rule named ``cc`` is defined with command ``gcc -o $out $ccopts $in``
  where ``$ccopts`` refers to the value of the global Ninja variable
  ``ccopts``.  Furthermore, if the rule is used in a Ninja goal, for ``$in`` /
  ``out`` the paths of used input file(s) / output file are substituted.

+ Sections ``goals.<goal>`` (at least one is mandatory):

  Either a *Shinobi* single or *Shinobi* multi goal with name ``<goal>`` is
  defined.  The following parameters are used:

  - Parameter ``rule`` (mandatory):

    name of the Ninja rule used by the goal.

  - Parameter ``in`` (*Shinobi* single goals only, mandatory):

    the path(s) of input file(s) for the goal.

  - Parameter ``ins`` (*Shinobi* multi goals only, mandatory):

    list of the paths of the input files for the goal.

  - Parameter ``out`` (mandatory):

    the path of the output file(s) for the goal.  For a *Shinobi* multi goal
    it is a template for the output file path of every bundled Ninja goal, and
    ``$in`` within the parameter value refers to the path of the respective
    input file.

  Example for a *Shinobi* multi goal:

  .. literalinclude:: ../examples/guess_nat/shinobi.conf
     :language: ini
     :lines: 20-28
     :emphasize-lines: 1,3,5,8

  *Shinobi* multi goal with name ``cc`` is defined.  The goal uses the rule
  with name ``cc``.  Its purpose is to compile all C implementation files into
  corresponding object files by using Ninja-Rule with name ``cc``.  For every
  C implementation file, the corresponding object file gets the same
  basename and extension ``o``, and it is stored in the subdirectory with
  relative path given by the expansion of ``%{paths.binsdir}%``.

  Example for a *Shinobi* single goal:

  .. literalinclude:: ../examples/guess_nat/shinobi.conf
     :language: ini
     :lines: 29-35
     :emphasize-lines: 1,3,5,7

  *Shinobi* single goal with name ``lnk_man`` is defined.  It uses the Ninja
  rule named ``lnk``.  Its purpose is to link all object files for the game
  variant with the interactive player to an executable.  This executable gets
  with filename from expanded ``%{paths.exe}%_interact``, and it is stored in
  directory with path from expanded ``%{paths.bindir_games}%``.

All other sections or parameters in a *Shinobi* configuration file are
optional and have no fixed meaning for *Shinobi*.  Such sections or parameters
can be used, for instance, to make the configuration more readable and
modular.  In our running example section ``paths`` has this purpose: its
parameters provide values of file and directory paths / names that are used
within other parameter values.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi
