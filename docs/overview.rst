..
 =============================================================================
 Title          : Simple build system based on Ninja - aka Shinobi

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-03-23

 Description    : Package overview.
 =============================================================================


===================================
General information for the package
===================================

.. toctree::
   :maxdepth: 1

   readme

   license

   changelog


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi
