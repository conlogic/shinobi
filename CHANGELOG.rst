..
 =============================================================================
 Title          : Simple build system based on Ninja - aka Shinobi

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-08-09

 Description    : Change log for the package.
 =============================================================================


=========
Changelog
=========


Version 4.0.3 (2022-08-09)
==========================

- Allow ``duconfig`` versions < 6.0.

- Commandline script file:`snob` is automatically generated.


Version 4.0.2 (2021-12-29)
==========================

- Add support for Python 3.10.

- Require version >= 3.0 of ``duconfig``.
  This version provides no functional changes, but has a correct and complete
  documentation for the new major version of ``duconfig``.


Version 4.0.1 (2021-10-28)
==========================

- Require version >= 2.2 of ``duconfigparser``.
  This allows to fix a parsing bug when using function call references with
  elements of list values.

- Require version >= 3.0dev9 of ``duconfig``.
  This version works with version >= 2.2 of ``duconfigparser`` to fix a
  parsing bug when using function call references with elements of list
  values.


Version 4.0 (2021-10-22)
========================

- Use the new standard functions for function value interpolation added in
  version 4.0dev9 in the ``guess_nat`` example.

- Update and review the usage part of the documentation, and improve its
  structure and content.


Version 4.0dev9 (2021-10-19)
============================

- Add a JSON format Shinobi configuration file for a variant of the
  ``fibonacci`` example.

- Add new standard functions for function value interpolation, and document
  them in the usage part of the documentation:

  - a :py:func:`find_ext_dir` variant :py:func:`find_ext_dirs` for a given
    list of directory paths.

  - function :py:func:`unique_list` to remove duplicate elements from a list.

  - function :py:func:`file_noext` to strip an extension from a given
    filepaths, and its variant :py:func:`files_noext` for a list of filepaths.

- Fix the ``guess.h`` header file for the ``guess_nat`` example.

- Update and rework the documentation:

  - Tighten the read me part of the documentation.

  - Fix leftovers in the usage parts.


Version 4.0dev8 (2021-10-14)
============================

- Add tests for function :py:func:`calc_config_fpath` from :py:mod:`cmdline`.

- Fix the bug when giving, at commandline, a relative configuration file path
  together with a base directory path, and add a test for it.

- Re-license this package: it now uses the 2 clauses BSD license instead of
  the LGPL, version 3.


Version 4.0dev7 (2021-10-11)
============================

- Require version >= 2.1 of ``duconfigparser``.
  This is stable version with good test coverage, improved code, and updated
  documentation.

- Require version >= 3.0dev8 of ``duconfig``.
  This version works with version >= 2.1 of ``duconfigparser``.


Version 4.0dev6 (2021-10-01)
============================

- Require version >= 3.0dev7 of ``duconfig`` to get its bug fixes.


Version 4.0dev5 (2021-09-30)
============================

- Require version >= 3.0dev6 of ``duconfig`` to get its bug fixes.


Version 4.0dev4 (2021-09-30)
============================

- Fix a ``CHANGELOG`` typo.

- Require version >= 2.1dev1 of ``duconfigparser``.
  This signals an error for missing delimiters for list values.

- Require version >= 3.0dev5 of ``duconfig``.
  This signals errors for a wrong encoding in configuration files, and
  (via version requirement for package ``duipoldict``) for using functions in
  function call references with a wrong number of arguments.


Version 4.0dev3 (2021-09-28)
============================

- Require version >= 3.0dev4 of ``duconfig``.

- Review code documentation to fix typos and adapt it to the current
  ``duipoldict`` / ``duconfig`` terminology.

- Make the error message for missing configuration file more generic.

- The section for Ninja variables in Shinobi configuration files is no longer
  mandatory.  Add an example and tests for this, too.

- Improved error messages for missing Ninja rules / goals in the
  configuration.

- In the usage documentation, make explicit which sections in Shinobi
  configurations with fixed meanings are mandatory and which are optional.


Version 4.0dev2 (2021-09-27)
============================

- Require version >= 3.0dev3 of ``duconfig``, and adapt to its API changes.
  Via the new version of its dependency ``duipoldict`` configuration parsing
  errors are fixed, and much improved debug logging and error messages are
  provided for reading Shinobi configuration files.

- Require version >= 2.0 of ``duconfigparser``.


Version 4.0dev1 (2021-09-09)
============================

- Require version >= 3.0dev1 of ``duconfig``.  It uses package ``duipoldict``
  instead of ``dumapinterpol`` for value interpolation.  With this change,
  Shinobi gets a much more powerful mechanism to read its configuration files
  with real parsing and much improved error messages.

- Remove option ``-p`` / ``--config-parameter`` to set configuration
  parameters for now.  It will be later re-added.

- Use the configuration parameter ``env.ninja_cmd`` to set the Ninja command
  in the Shinobi configuration files for the ``fibonacci`` example.


Version 3.0.1 (2021-08-23)
==========================

- More robust call of :py:func:`run_shinobi_fct` in :py:func:`main` from
  :py:mod:`cmdline`.


Version 3.0 (2021-08-19)
========================

- Fix script to generate test data files, and move it below the ``tools``
  subdirectory.

- Use standard delimiters from ``duconfig`` for list values in Shinobi
  configuration files in CONF format, too.

- Shinobi now looks for a ``shinobi.conf`` or a ``shinobi.json`` configuration
  file (in this order) in the base directory.  Beside this there is a new
  option ``-c`` / ``--config-file`` to specify an alternative path for the
  Shinobi configuration file.

- The configuration parameter ``env.basedir`` to set the path for base
  directory is gone.  Instead of it there is a new option ``-b`` /
  ``--base-dir`` to set the path for the base directory.  The default base
  directory is the current working directory.

- Add new configuration parameter ``env.ninja_cmd`` to set the Ninja command
  to be used.

- Improve tests by using more diverse test data.

- Fix a glitch in documentation: catch up the renaming of option ``-c`` to
  ``-p`` in version 2.0.


Version 2.2 (2021-08-12)
========================

- Clarify the documentation for some of the functions in :py:mod:`functions`.

- Add function :py:func`common_lists` to :py:mod:`functions` as function
  available for function call interpolation in Shinobi configuration files.


Version 2.1 (2021-08-11)
========================

- Make test framework more flexible by decoupling example variants from the
  source files, and extend criteria for (non-)existing files for build errors.

- Use multiple build errors for testing.


Version 2.0 (2021-08-10)
========================

- Simplification by getting rid of Shinobi build stages.

- Use specific exceptions, if sensible, instead a once-for-all
  :py:exc:`ShinobiError`.

- Update version of dependency ``duconfig`` to >= 2.0.

- Option ``-k`` / ``--keep-going`` becomes a on/off option again, because the
  remaining single Ninja call need for one Shinobi call does not need
  different keep going modes any longer.

- Change short option to set configuration parameters from ``-c`` to ``-p``.


Version 1.5 (2021-08-03)
========================

- Add function :py:func`diff_lists` to :py:mod:`functions` as function
  available for function call interpolation in Shinobi configuration files.


Version 1.4.4 (2021-08-03)
==========================

- Update versions of dependency ``dubasiclog`` to >= 1.1.

- Provide function (from ``dubasiclog`` with version >= 1.1) in
  :py:mod:`utilities` to disable logging, and use it to safely silence all
  tests.

- Partial fix for test data generation: now all generated test data are at
  least correct for stage 1.

- Fix interpolation in Shinobi configuration to derive a Ninja recipe: this
  interpolation for the recipe of a certain stage ``<s>`` can only be done
  *after* the stages before ``<s>`` are completed.


Version 1.4.3 (2021-07-22)
==========================

- Test modules get their directory paths more simple.


Version 1.4.2 (2021-07-21)
==========================

- Improve an error message when calling Ninja fails.

- Add tests to use an alternative Ninja command in :py:func:`run_ninja` from
  :py:mod:`ninja`.


Version 1.4.1 (2021-07-21)
==========================

- Fix a `cmdline` test when ``duargparse`` is not installed.

- Check in tests expecting a :py:exc:`ShinobiError` the reason code of the
  exception, too.

- Improve code coverage of the tests by adding even more unit tests -
  in particular, by adding the doc tests for :py:mod:`functions` as unit
  tests.

- Handle also a :py:exc:`OSError` raised in `run_shinobi_for_config`
  :py:mod:`command`.


Version 1.4 (2021-07-19)
========================

- Complete overhaul of all tests:

  - The code structure of the tests is improved a lot.

  - The test coverage is substantially increased.

  - The tests are based on test data that is generated from the examples.

  - The tests are completely silenced, and all expected results are
    automatically checked.

- Add more built Ninja recipe files to the documentation.

- Fix a ``CHANGELOG`` glitch.

- Fix argument passing for :py:func:`run_shinobi_for_config` from
  :py:mod:`command`, and make it more robust.

- Fix doc string for :py:func:`main` from :py:mod:`cmdline`, and allow to pass
  additional arguments to py:func:`run_shinobi`.


Version 1.3 (2021-07-11)
========================

- Fix a ``CHANGELOG`` glitch.

- Replace the primes example implemented in C++ by a number guessing game
  example implemented in C - both in the tests and in the documentation.
  The old primes example in C++ is completely removed.

- Make the Fibonacci number example consistent with the new number guessing
  game example.

- No special source files needed anymore to test build failures - use the
  number guessing game, and manipulate a C implementation file on the fly.


Version 1.2 (2021-07-09)
========================

- Enhance :py:func:`run_ninja` from :py:mod:`ninja` to pass additional keyword
  arguments to :py:func:`subprocess.run` and to return the result of the
  latter call.

- Enhance :py:func:`run_shinobi` and :py:func:`run_shinobi_for_config` from
  :py:mod:`command` like :py:func:`run_ninja` and handle Ninja call errors
  without :py:exc:`NinjaCallError`.

- Fix some ``CHANGELOG`` glitches.

- Silence the tests for :py:func:`ninja.run_ninja`.


Version 1.1.1 (2021-07-08)
==========================

- Simplify to set of additional environment variables.

- Move some helper functions from :py:mod:`functions` into
  :py:mod:`utilities`.

- Improve code structure and documentation, and fix all glitches found.


Version 1.1 (2021-07-08)
========================

- Rename :py:class:`Config` methods from module :py:mod:`Config` that are
  related to Ninja recipes: ``<meth>`` -> ``ninja_<meth>``.

- Decouple interfaces in the following former modules from :py:mod:`config`:

  - :py:mod:`recipes`

  - :py:mod:`envvars`

- Keep only former method :py:meth:`write_file` from :py:class:`NinjaRecipe`
  in former :py:mod:`recipes` as standalone function.

- Merge the (remaining) content of the following modules into new module
  :py:mod:`ninja`:

  - :py:mod:`recipes`

  - :py:mod:`envvars`

  - :py:mod:`ninjacall`


Version 1.0 (2021-07-07)
========================

- Add examples usable as doc tests, too, to the functions available for
  configuration files.

- Fix some glitches for the functions available for configuration files.

- Fix some test glitches.

- Fix a typo in the ``CHANGELOG`` file.

- Add tests for modules :py:mod:`config`, :py:mod:`envvars`,
  :py:mod:`ninjacall`  and :py:mod:`recipes`.

- Update development status (via ``classifiers`` for ``setup``).


Version 0.6.3 (2021-07-06)
==========================

- Switch to simple C-based tests for build failures.


Version 0.6.2 (2021-07-05)
==========================

- Update versions of dependency ``duconfigparser`` to >= 1.1 and of dependency
  ``duconfigparser`` to >= 1.1.

- Regain the old API for CONF configuration files.


Version 0.6.1 (2021-07-02)
==========================

- Use more :py:mod:`pathlib` in the tests and package tools.

- Fix some function doc strings.


Version 0.6 (2021-07-02)
========================

- Simplify :py:class:`config.Config`.

- Purge some NinjaCob leftovers in the tests.

- Stick to naming convention for test cases.

- Allow a different tool name in the help message.


Version 0.5.1 (2021-07-01)
==========================

- Fix typos in some function doc strings.

- Fix development status (via ``classifiers`` for ``setup``).

- Migrate package tools to ``f"..."``-strings and :py:mod:`pathlib`.

- Migrate package itself to ``f"..."``-strings and :py:mod:`pathlib`.

- Drop support for Python 3.5, since ``f"..."``-strings are used (via
  ``classifiers`` for ``setup``).


Version 0.5 (2021-06-29)
========================

- Add more tests (failing builds + ``ignore_errors`` parameter, ``nr_tasks``
  parameter).

- Fix handling of the ``nr_tasks`` parameter.

- Use a specific exception :py:exc:`NinjaCallError` for errors during Ninja
  calls.

- Add a strong keep-going mode used if multiple ``-k`` / ``--keep-going``
  options are given.

- Switch to semantic versions for the dependencies.


Version 0.4.3 (2021-03-25)
==========================

- Enhance ``setup`` configuration for Sphinx.

- Further improvements for the documentation (``usage.rst``) using  feedback
  from review (thanks to Christian Reich).


Version 0.4.2 (2021-03-23)
==========================

- Fix some glitches in the *Shinobi* documentation (spelling and formatting).

- Improve some formulations in the *Shinobi* documentation.


Version 0.4.1 (2021-03-23)
==========================

- Fix a glitch in the ``README`` file.

- Add a link to the online copy of the *Shinobi* documentation.


Version 0.4 (2021-03-23)
========================

- Rename "tinyMk" to "Shinobi" since original name is already use by others.
  This includes all identifiers and names using the package name.


Version 0.3.3 (2021-03-22)
==========================

- Move helper functions needed by :py:mod:`functions` into this very module.


Version 0.3.2 (2021-03-22)
==========================

- Further improvements for the documentation (``usage.rst``) using  feedback
  from review by Jens Rapp.

- Update version of dependency ``dubasiclog`` to >= 1.0.

- Fix fallback to :py:mod:`argparse.ArgumentParser`.


Version 0.3.1 (2021-03-18)
==========================

- Use a :py:exc:`TinyMkError` for missing configuration parameters.

- Fix glitches and improve the documentation after proofreading and feedback
  from review (many thanks to Jens Rapp).

- Move the example content in the read me section from the ``README.rst`` file
  into ``docs/readme.rst`` since it is not properly rendered by GitLab.


Version 0.3 (2021-03-17)
========================

- Use test data as examples, and move them into the ``examples``
  subdirectory.

- Add Sphinx-based documentation how to use *tinykMk*.

- Fix formatting in documentation strings of some functions and methods.1

- Rename two functions from :py:mod:`functions` to be used within the values
  of configuration parameters for consistency:

  - :py:func:`files_dir` -> :py:func:`files_dirs`

  - :py:func:`files_base` -> :py:func:`files_bases`


Version 0.2.1 (2021-03-12)
==========================

- Remove duplicate code in :py:mod:`utilities`.


Version 0.2 (2021-03-12)
========================

- Document return code of :py:func:`main` function in :py:mod:`cmdline`, and
  check it its tests, too.

- Make the :py:func:`main` function reusable by adding parameters for the
  parts supposed to be changeable.

- Unify error handling by using :py:exc:`TinyMkError` for commandline parsing
  errors, and print its error message instead of raising the exception.


Version 0.1 (2021-03-09)
========================

- Let :py:func:`find_ext_in_dir` return a sorted list of found paths.

- Add a Python script with wrapper ``tmk`` to run the *tinyMk* command from
  the commandline.

- Add some useful tests for the commandline interface.


Version 0.1m12.1 (2021-02-20)
=============================

Milestone 12 (fix 1) for initial version:

- More reliable check of the ``ninja`` command.


Version 0.1m12 (2021-02-20)
===========================

Milestone 12 for initial version:

- Raise an :py:exc:`TinyMkError` if the :py:mod:`ninja_syntax` module cannot
  be imported.

- Raise an :py:exc:`TinyMkError` if the ``ninja`` command cannot be found.

- Sort expanded multi goals by their expanded ``$ins``.


Version 0.1m11 (2021-02-08)
===========================

Milestone 11 for initial version:

- Update required version of ``duconfig`` to >= 0.4 to use priority items for
  overwriting parameters when running commands.

- Generic mechanism to overwrite parameters when running commands; therefore a
  special handling of ``env.basedir`` is not longer needed.


Version 0.1m10.2 (2021-01-31)
=============================

Milestone 10 (fix 2) for initial version:

- Update required version of ``duconfig`` to >= 0.3.1 for improved
  :py:exc:`ConfigError`.

- Improve :py:exc:`TinyMkError` (improve its :py:meth:`__str__` method's
  logic).


Version 0.1m10.1 (2021-01-29)
=============================

Milestone 10 (fix 1) for initial version:

- Update required version of ``duconfig`` to >= 0.3 to keep the code for
  reading configuration files simple.

- Simplify the logic to calculate the base directory path used.

- Simplify the logic for configuration parameters in ``command``.


Version 0.1m10 (2021-01-29)
===========================

Milestone 10 for initial version:

- Update required version of ``duconfig`` to >= 0.2 needed to set values in a
  configuration object.

- Add to :py:func:`run_tinymk[for_config]` functions in :py:mod:`command` an
  additional argument to overwrite the path of base directory.

- Add unit tests for the new base directory path parameter, and for using
  relative paths within the configuration.

- Fix recipe creation for relative paths in the configuration.


Version 0.1m9 (2021-01-26)
==========================

Milestone 9 for initial version:

- New standard functions :py:func:`join_lists`, :py:func:`find_ext_in_dir`,
  :py:func:`file_dir` and :py:func:`files_dir` available for function call
  interpolation in configuration files.


Version 0.1m8.1 (2021-01-22)
============================

Milestone 8 (fix 1) for initial version:

- Improve unit tests a lot.

- Fix passing of functions for function call interpolation in configuration
  files.


Version 0.1m8 (2021-01-19)
==========================

Milestone 8 for initial version:

- Use package ``duconfig`` for configuration.


Version 0.1m7 (2021-01-18)
==========================

Milestone 7 for initial version:

- Use package ``dubasiclog`` for logging.

- Add some unit tests (for the :py:mod:`command` module).


Version 0.1m6.5 (2021-01-06)
============================

Milestone 6 (fix 5) for initial version:

- Update copyright for 2021.


Version 0.1m6.4 (2021-01-04)
============================

Milestone 6 (fix 4) for initial version:

- Make some error messages more informative.


Version 0.1m6.3 (2021-01-04)
============================

Milestone 6 (fix 3) for initial version:

- Add some debug logging for parsing CONF files.

- Fix for arbitrary section order in CONF files.


Version 0.1m6.2 (2020-12-30)
============================

Milestone 6 (fix 2) for initial version:

- Fix dry run for multi staged goals.


Version 0.1m6.1 (2020-12-30)
============================

Milestone 6 (fix 1) for initial version:

- Make stage logic more robust.


Version 0.1m6 (2020-12-30)
==========================

Milestone 6 for initial version:

- Add support for staged goals.


Version 0.1m5.1 (2020-12-30)
============================

Milestone 5 (fix 1) for initial version:

- Split main function in 2 components for reusability.


Version 0.1m5 (2020-12-30)
==========================

Milestone 5 for initial version:

- Add a function for the main functionality.


Version 0.1m4 (2020-12-21)
==========================

Milestone 4 for initial version:

- Handling CONF configuration files, too.

- Fix the documentation of some interpolation code.


Version 0.1m3.1 (2020-12-17)
============================

Milestone 3 (fix 1) for initial version:

- Add more log messages, and improve some former ones.


Version 0.1m3 (2020-12-16)
==========================

Milestone 3 for initial version:

- Add debug logging for configuration value interpolation.


Version 0.1m2 (2020-12-16)
==========================

Milestone 2 for initial version:

- Change terminology: rename "build" -> "goal".


Version 0.1m1 (2020-12-12)
==========================

Milestone 1 for initial version:

- Handling JSON configuration files using both key and function call
  interpolation.

- Basic logging for important steps.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi NinjaCob
