# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-03-23
#
# Description    : See documentation string below.
# ============================================================================

'''
Init module for the unit test package of package ``shinobi``.
'''


# This turns the ``tests`` directory into a Python package.


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
