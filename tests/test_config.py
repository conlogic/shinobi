# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`config` in the ``shinobi`` package.
'''


from . import helpers

import shinobi.functions as functions
import shinobi.config as config


# Classes
# =======


class base_config_TC(helpers.base_shinobi_TC):
    '''
    Base class for tests of the :py:mod:`config` module.
    '''
    def setUp(self):
        super().setUp()

        # Add parameter for the base directory.
        self.cfg_args = {'fcts': functions.CFG_FILE_FUNCTIONS}


    def prep_test_no_cfg(self, example, variant=None, use_json=False):
        '''
        Prepare a test using source code and Shinobi configuration file in
        CONF format (if `use_json` is ``False``) or JSON format (if `use_json`
        is ``True``) from variant `variant` (or standard if `variant` is
        ``None``) of example `example` below the base test directory.

        :return: destination path of the copied configuration file, if a
                 respective Shinobi configuration file exists, or ``None``
                 otherwise.
        '''
        # Clean up the test base directory, and copy source files.
        self.cp_example_src(example, ensure_clean=True)
        # Prepare configuration file.
        cfg_fpath = self.cp_example_cfg(example, variant, use_json)

        return cfg_fpath


    def prep_test_with_cfg(self, example, variant=None, use_json=False,
                           cfg_args=None):
        '''
        Prepare a test using source code and Shinobi configuration in CONF
        format (if `use_json` is ``False``) or JSON format (if `use_json` is
        ``True``) from variant `variant` (or standard if `variant` is
        ``None``) of example `example` below the base test directory.

        A corresponding configuration object is prepared with the respective
        Shinobi configuration file read.  A not-``None`` dictionary `cfg_args`
        is used for the keyword arguments for the object's initialization
        instead of the standard ones.

        :return: the configuration object created if there was a respective
                 Shinobi configuration file, or ``None`` otherwise.
        '''
        # Clean up the test base directory, and copy the example source files,
        # an prepare a configuration file.
        cfg_fpath = self.prep_test_no_cfg(example, variant, use_json)

        # Create an appropriate configuration object, and read the
        # configuration file.
        if cfg_args is None:
            cfg_args = self.cfg_args
        cfg_obj = self.mk_cfg(cfg_fpath, cfg_args)

        return cfg_obj


    def mk_example_variant_test_data(self, example, variant=None):
        '''
        Make test data from variant `variant` (or standard if `variant` is
        ``None``) of example `example`.

        :return: list of paths of the written test data files.
        '''
        # Prepare test and configuration.
        raw_cfg = self.prep_test_with_cfg(example, variant)

        # Write data of the configuration.
        # - Nested raw data.
        cfg_nest_raw_data_fpath = self._test_data_fpath(
            self.cfg_nest_fbase, example, variant=variant, raw=True)
        raw_cfg_nest_data = dict(raw_cfg)
        helpers.write_json_file(raw_cfg_nest_data, cfg_nest_raw_data_fpath)
        data_fpaths = [cfg_nest_raw_data_fpath]
        # - Nested expanded data.
        cfg_nest_epd_data_fpath = self._test_data_fpath(
            self.cfg_nest_fbase, example, variant=variant, raw=False)
        with helpers.with_test_wd():
            epd_cfg_nest_data = {
                p: raw_cfg.get(p, raw=False) for p in raw_cfg.keys()}
        helpers.write_json_file(epd_cfg_nest_data, cfg_nest_epd_data_fpath)
        data_fpaths.append(cfg_nest_epd_data_fpath)

        # Write recipes data.
        rcp_raw_data_fpath = self._test_data_fpath(
            self.rcp_fbase, example, variant=variant, raw=True)
        rcp_epd_data_fpath = self._test_data_fpath(
            self.rcp_fbase, example, variant=variant, raw=False)
        rcp_raw_data = {}
        rcp_epd_data = {}
        whats = [
            'ninja_variables', 'ninja_rules', 'ninja_goals', 'env_variables']
        with helpers.with_test_wd():
            for w in whats:
                cfg_meth_w = getattr(raw_cfg, w)
                rcp_raw_data[w] = cfg_meth_w(raw=True)
                rcp_epd_data[w] = cfg_meth_w(raw=False)
        helpers.write_json_file(rcp_raw_data, rcp_raw_data_fpath)
        data_fpaths.append(rcp_raw_data_fpath)
        helpers.write_json_file(rcp_epd_data, rcp_epd_data_fpath)
        data_fpaths.append(rcp_epd_data_fpath)

        return data_fpaths


class Config_read_file_TC(base_config_TC):
    '''
    Tests for :py:meth:`read_file` of :py:class:`Config`.
    '''


    def test_conf(self):
        '''
        Test for a Shinobi CONF configuration file.
        '''
        example = 'guess_nat'
        cfg = config.Config(**self.cfg_args)
        cfg_fpath = self.prep_test_no_cfg(example, use_json=False)
        cfg.read_file(cfg_fpath)
        self.check_example_cfg_data(cfg, example)


    def test_json(self):
        '''
        Test for a Shinobi JSON configuration file.
        '''
        example = 'guess_nat'
        cfg = config.Config(**self.cfg_args)
        cfg_fpath = self.prep_test_no_cfg(example, use_json=True)
        cfg.read_file(cfg_fpath)
        self.check_example_cfg_data(cfg, example)


class base_Config_recipes_TC(base_config_TC):
    '''
    Base class for tests for methods of :py:class:`Config` providing data to
    generate Ninja recipes for a given configuration.
    '''
    def check_test_result(self, data, example, what, variant=None, raw=True):
        '''
        Check result raw (if `raw` is ``True``) or expanded (if `raw` is
        ``False``) data `data` for recipes of type `what` from variant
        `variant` (or standard if `variant` is ``None``) of example `example`.

        :raise: a :py:exc:`failureException` if the test fails.
        '''
        # Calculate the path for the test data file.
        data_fpath = self._test_data_fpath(
            self.rcp_fbase, example, variant, raw)

        # Read the test data file.
        data_dict = helpers.read_json_file(data_fpath)

        # Compare the data.
        what_data = data_dict[what]
        self.assertDictEqual(data, what_data)


class Config_ninja_variables_TC(base_Config_recipes_TC):
    '''
    Tests for :py:meth:`ninja_variables` of :py:class:`Config`.
    '''
    def check_test_result(self, data, example, variant=None, raw=True):
        '''
        Variant of super class :py:meth:`check_test_data` for
        :py:meth:`ninja_variables`.
        '''
        super().check_test_result(
            data, example, 'ninja_variables', variant, raw)


    def test_raw_data(self):
        '''
        Test to get raw data.
        '''
        example = 'guess_nat'
        cfg = self.prep_test_with_cfg(example)
        res_vars = cfg.ninja_variables(raw=True)
        self.check_test_result(res_vars, example, raw=True)


    def test_epd_data_abs(self):
        '''
        Test to get expanded value with absolute paths.
        '''
        example = 'fibonacci'
        variant = 'abs'
        cfg = self.prep_test_with_cfg(example, variant)
        res_vars = cfg.ninja_variables(raw=False)
        self.check_test_result(res_vars, example, variant, raw=False)


    def test_epd_data_rel(self):
        '''
        Test to get expanded value with relative paths.
        '''
        example = 'fibonacci'
        variant = 'rel'
        cfg = self.prep_test_with_cfg(example, variant)
        with helpers.with_test_wd():
            res_vars = cfg.ninja_variables(raw=False)
        self.check_test_result(res_vars, example, variant, raw=False)


    def test_epd_data_nvr(self):
        '''
        Test to get expanded value with relative paths.
        '''
        example = 'fibonacci'
        variant = 'nvr'
        cfg = self.prep_test_with_cfg(example, variant)
        with helpers.with_test_wd():
            res_vars = cfg.ninja_variables(raw=False)
        self.check_test_result(res_vars, example, variant, raw=False)


class Config_ninja_rules_TC(base_Config_recipes_TC):
    '''
    Tests for :py:meth:`ninja_rules` of :py:class:`Config`.
    '''
    def check_test_result(self, data, example, variant=None, raw=True):
        '''
        Variant of super class :py:meth:`check_test_data` for
        :py:meth:`ninja_rules`.
        '''
        super().check_test_result(data, example, 'ninja_rules', variant, raw)


    def test_raw_data(self):
        '''
        Test to get raw data.
        '''
        example = 'guess_nat'
        cfg = self.prep_test_with_cfg(example)
        res_rules = cfg.ninja_rules(raw=True)
        self.check_test_result(res_rules, example, raw=True)


    def test_epd_data_abs(self):
        '''
        Test to get expanded value with absolute paths.
        '''
        example = 'fibonacci'
        variant = 'abs'
        cfg = self.prep_test_with_cfg(example, variant)
        res_rules = cfg.ninja_rules(raw=False)
        self.check_test_result(res_rules, example, variant, raw=False)


    def test_epd_data_rel(self):
        '''
        Test to get expanded value with relative paths.
        '''
        example = 'fibonacci'
        variant = 'rel'
        cfg = self.prep_test_with_cfg(example, variant)
        with helpers.with_test_wd():
            res_rules = cfg.ninja_rules(raw=False)
        self.check_test_result(res_rules, example, variant, raw=False)


class Config_ninja_goals_TC(base_Config_recipes_TC):
    '''
    Tests for :py:meth:`ninja_goals` of :py:class:`Config`.
    '''
    def check_test_result(self, data, example, variant=None, raw=True):
        '''
        Variant of super class :py:meth:`check_test_data` for
        :py:meth:`ninja_goals`.
        '''
        super().check_test_result(data, example, 'ninja_goals', variant, raw)


    def test_raw_data(self):
        '''
        Test to get raw data.
        '''
        example = 'guess_nat'
        cfg = self.prep_test_with_cfg(example)
        res_goals = cfg.ninja_goals(raw=True)
        self.check_test_result(res_goals, example, raw=True)


    def test_epd_data_abs(self):
        '''
        Test to get expanded value with absolute paths.
        '''
        example = 'fibonacci'
        variant = 'abs'
        cfg = self.prep_test_with_cfg(example, variant)
        res_goals = cfg.ninja_goals(raw=False)
        self.check_test_result(res_goals, example, variant, raw=False)


    def test_epd_data_rel(self):
        '''
        Test to get expanded value with absolute paths.
        '''
        example = 'fibonacci'
        variant = 'rel'
        cfg = self.prep_test_with_cfg(example, variant)
        with helpers.with_test_wd():
            res_goals = cfg.ninja_goals(raw=False)
        self.check_test_result(res_goals, example, variant, raw=False)


class Config_env_variables_TC(base_Config_recipes_TC):
    '''
    Tests for :py:meth:`env_variables` of :py:class:`Config`.
    '''
    def check_test_result(self, data, example, variant=None, raw=True):
        '''
        Variant of super class :py:meth:`check_test_data` for
        :py:meth:`env_variables`.
        '''
        super().check_test_result(
            data, example, 'env_variables', variant, raw)


    def test_raw_data(self):
        '''
        Test to get raw data.
        '''
        example = 'guess_nat'
        cfg = self.prep_test_with_cfg(example)
        env_vars = cfg.env_variables(raw=True)
        self.check_test_result(env_vars, example, raw=True)


    def test_epd_data_abs(self):
        '''
        Test to get expanded value with absolute paths.
        '''
        example = 'fibonacci'
        variant = 'abs'
        cfg = self.prep_test_with_cfg(example, variant)
        env_vars = cfg.env_variables(raw=False)
        self.check_test_result(env_vars, example, variant, raw=False)


    def test_epd_data_rel(self):
        '''
        Test to get expanded value with relative paths.
        '''
        example = 'fibonacci'
        variant = 'rel'
        cfg = self.prep_test_with_cfg(example, variant)
        with helpers.with_test_wd():
            env_vars = cfg.env_variables(raw=False)
        self.check_test_result(env_vars, example, variant, raw=False)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
