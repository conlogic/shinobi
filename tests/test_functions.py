# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit test wrapper to run the doc tests for module :py:mod:`functions` in the
``shinobi`` package.
'''


import doctest

import shinobi.functions as functions


def load_tests(loader, tests, ignore):
    '''
    Let :py:class:`TestLoader` `loader` add the doc tests for
    :py:mod:`functions` add to :py:class:`TestSuite` `tests`.

    Pattern `ignore` to ignore tests is ignored for these doc tests.

    :return: the enhanced test suite.
    '''
    tests.addTests(doctest.DocTestSuite(functions))

    return tests


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
