# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`ninja` in the ``shinobi`` package.
'''


import sys
import stat

import shinobi.ninja as ninja

from . import helpers


# Classes
# =======


class base_recipe_TC(helpers.base_shinobi_TC):
    '''
    Base class for tests of :py:mod:`ninja` module.
    '''
    def prep_recipe_data(self, example, variant=None):
        '''
        Prepare Ninja recipe data from variant `variant` (or standard if
        `variant` is ``None``) of example `example` needed to write a
        respective Ninja recipe file.

        :return: pair of a tuple usable as :py:func:`write_ninja_recipe`
                 arguments, and the dictionary with the environment variables
                 to set before the Ninja run.
        '''
        # Load test data file with relevant recipe data.
        rcp_data_fpath = self._test_data_fpath(
            self.rcp_fbase, example, variant, raw=False)
        rcp_data = helpers.read_json_file(rcp_data_fpath)

        # Group recipe data for :py:func:`write_ninja_recipe` calls.
        rcp_data_args = (
            rcp_data['ninja_variables'],
            rcp_data['ninja_rules'],
            rcp_data['ninja_goals']
        )

        return (rcp_data_args, rcp_data['env_variables'])


    def prep_run_data(self, example=None, variant=None, build_error=None,
                      dst_base_dpath=None):
        '''
        Prepare data for a Ninja run in destination base directory
        `dst_base_dpath`either

        + from variant `variant` (or standard if `variant` is ``None``) of
          example `example`, if `example` is not ``None``, or
        + for build error `build_error` with its example and variant, if
          `build_error` is not ``None``.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        :return: pair of the environment variables to be set before the Ninja
                 run, and the path for Ninja recipe file to be run.
        '''
        # Prepare test data.
        # - Clean up the test base directory copy sources, and prepare a build
        #   error, if wanted.
        example = self.cp_example_src(
            example, build_error, dst_base_dpath, ensure_clean=True)
        # - Copy Ninja recipe.
        rcp_fpath = self.cp_example_recipe(example, variant, dst_base_dpath)

        # Calculate the environment variables to be set.
        (_, env_vars) = self.prep_recipe_data(example, variant)

        return (env_vars, rcp_fpath)


    def mk_example_variant_test_data(self, example, variant=None):
        '''
        Make test data from variant `variant` (or standard if `variant` is
        ``None``) of example `example`.

        :return: list of paths of the written test data files.
        '''
        # Generate the test data files.
        data_fpaths = []

        # - Ninja recipe file.
        rcp_fpath = self._example_path(self.rcp_fname, example, variant)
        (rcp_data_args, _) = self.prep_recipe_data(example, variant)
        ninja.write_ninja_recipe(rcp_fpath, *rcp_data_args)
        # Restore placeholders in the recipe file.
        for (p, v) in self.data_replaces:
            helpers.replace_in_file(rcp_fpath, v, p)
        data_fpaths.append(rcp_fpath)

        # - Files with info about the files generated in the Ninja run.
        # Run Ninja.
        (env_vars, rcp_fpath) = self.prep_run_data(example, variant)
        ninja.run_ninja(
            rcp_fpath, env_vars=env_vars, base_dpath=self.test_base_dpath,
            add_run_args=self.silent_run_args)
        # Collect the produced binary files.
        bin_fpaths = helpers.files_in_subdir(
            self.test_base_dpath, self.bin_sdname, mk_relative=True)
        bin_fpaths_strs = [str(f) for f in bin_fpaths]
        # Save the paths of the produced binary files.
        out_fpath = self._test_data_fpath(self.out_fbase, example, variant)
        helpers.write_json_file(bin_fpaths_strs, out_fpath)
        data_fpaths.append(out_fpath)

        return data_fpaths


class write_ninja_recipe_TC(base_recipe_TC):
    '''
    Tests for :py:func:`write_ninja_recipe`.
    '''
    def setUp(self):
        super().setUp()

        # Path for the Ninja recipe file to be written.
        self.rcp_fpath = self.test_base_dpath / self.rcp_fname


    def prep_test(self, example, variant=None, dst_base_dpath=None):
        '''
        Prepare test with ninja recipe data from variant `variant` (or
        standard if `variant` is ``None``) of example `example` needed to
        write a respective Ninja recipe file.

        For a not-``None`` path `dst_base_dpath` the directory with this path
        is used as destination base directory instead of the test base
        directory.

        :return: tuple usable as :py:func:`write_ninja_recipe` arguments.
        '''
        if dst_base_dpath is None:
            dst_base_dpath = self.test_base_dpath

        # Clean up the destination base directory.
        helpers.ensure_clean_dir(dst_base_dpath)
        # Prepare recipe data.
        (rcp_data_args, _) = self.prep_recipe_data(example, variant)

        return rcp_data_args


    def test_read_only_dir(self):
        '''
        Test using a read-only directory for the Ninja recipe file.
        '''
        # Standard test preparation.
        example = 'fibonacci'
        variant = 'rel'
        base_dpath = self.test_base_dpath / 'foo'
        rcp_data_args = self.prep_test(example, variant, base_dpath)
        # Make the base directory read-only.
        base_dpath.chmod(stat.S_IRUSR)
        rcp_fpath = base_dpath / self.rcp_fname
        try:
            with self.assertRaises(ninja.NinjaRecipeError):
                ninja.write_ninja_recipe(rcp_fpath, *rcp_data_args)
        finally:
            # Make the base directory again accessible.
            base_dpath.chmod(stat.S_IRWXU)


    def test_missg_ninja_syntax(self):
        '''
        Test with failing :py:mod:`ninja_syntax` import.
        '''
        example = 'guess_nat'
        rcp_data_args = self.prep_test(example)
        # Ensure that :py:mod:`ninja_syntax` is not loaded and cannot be
        # loaded.
        if 'ninja_syntax' in sys.modules.keys():
            del sys.modules['ninja_syntax']
        sys_path_bak = sys.path
        sys.path = []
        with self.assertRaises(ninja.NinjaRecipeError):
            ninja.write_ninja_recipe(self.rcp_fpath, *rcp_data_args)
        sys.path = sys_path_bak


    def test_default(self):
        '''
        Test using defaults.
        '''
        example = 'guess_nat'
        rcp_data_args = self.prep_test(example)
        ninja.write_ninja_recipe(self.rcp_fpath, *rcp_data_args)
        self.check_recipe_file(self.rcp_fpath, example)


    def test_no_ninja_vars(self):
        '''
        Test using no Ninja variables.
        '''
        example = 'fibonacci'
        variant = 'nvr'
        rcp_data_args = self.prep_test(example, variant)
        ninja.write_ninja_recipe(self.rcp_fpath, *rcp_data_args)
        self.check_recipe_file(self.rcp_fpath, example, variant)


    def test_oth_header(self):
        '''
        Test using another header for the Ninja recipe file.
        '''
        header = 'This is a Ninja build recipe.'
        example = 'guess_nat'
        rcp_data_args = self.prep_test(example)
        ninja.write_ninja_recipe(
            self.rcp_fpath, *rcp_data_args, header=header)
        self.check_recipe_file(self.rcp_fpath, example, header=header)


class run_ninja_TC(base_recipe_TC):
    '''
    Tests for :py:func:`write_ninja_recipe`.
    '''
    def setUp(self):
        super().setUp()

        # Path for the Ninja recipe file to be written.
        self.rcp_fpath = self.test_base_dpath / self.rcp_fname


    def test_ninja_cmd_error(self):
        '''
        Test with an erroneous Ninja command.
        '''
        example = 'guess_nat'
        ninja_cmd = 'foo'
        (env_vars, rcp_fpath) = self.prep_run_data(example)
        with helpers.with_test_wd():
            with self.assertRaises(ninja.NinjaCallError):
                ninja.run_ninja(
                    rcp_fpath, env_vars=env_vars, ninja_cmd=ninja_cmd,
                    add_run_args=self.silent_run_args)


    def test_build_error(self):
        '''
        Test with a build error.
        '''
        build_error = self.build_err_total
        variant = 'rel'
        (env_vars, rcp_fpath) = self.prep_run_data(
            build_error=build_error, variant=variant)
        with helpers.with_test_wd():
            with self.assertRaises(ninja.NinjaCallError):
                ninja.run_ninja(
                    rcp_fpath, env_vars=env_vars,
                    add_run_args=self.silent_run_args)
        self.check_output_files(build_error=build_error, variant=variant)


    def test_default(self):
        '''
        Test using defaults.
        '''
        example = 'guess_nat'
        (env_vars, rcp_fpath) = self.prep_run_data(example)
        with helpers.with_test_wd():
            ninja.run_ninja(
                rcp_fpath, env_vars=env_vars,
                add_run_args=self.silent_run_args)
        self.check_output_files(
            example, exp_ex_out_fpaths=None, exp_noex_out_fpaths=[])


    def test_dry_run(self):
        '''
        Test for a dry run.
        '''
        example = 'guess_nat'
        (env_vars, rcp_fpath) = self.prep_run_data(example)
        with helpers.with_test_wd():
            ninja.run_ninja(
                rcp_fpath, env_vars=env_vars, dry_run=True,
                add_run_args=self.silent_run_args)
        self.check_output_files(
            example, exp_ex_out_fpaths=[], exp_noex_out_fpaths=None)


    def test_oth_ninja_cmd(self):
        '''
        Test using an alternative Ninja command.
        '''
        # Standard test preparation.
        example = 'guess_nat'
        (env_vars, rcp_fpath) = self.prep_run_data(example)
        # Add a Ninja call for testing.
        ninja_cmd_fpath = self.test_base_dpath / 'fake_ninja.py'
        ninja_cmd = f"python3 {ninja_cmd_fpath}"
        my_fpath = self.test_base_dpath / 'foobar'
        with open(ninja_cmd_fpath, 'wt') as ninja_cmd_fobj:
            ninja_cmd_fobj.write('from shutil import copy\n')
            ninja_cmd_fobj.write(f"copy('{rcp_fpath}', '{my_fpath}')\n")
        # Call Ninja, and check result.
        with helpers.with_test_wd():
            ninja.run_ninja(
                rcp_fpath, env_vars=env_vars, ninja_cmd=ninja_cmd,
                add_run_args=self.silent_run_args)
        self.check_output_files(
            example, exp_ex_out_fpaths=[my_fpath], exp_noex_out_fpaths=None)


    def test_nr_tasks(self):
        '''
        Test with a given number of parallel tasks.
        '''
        example = 'guess_nat'
        (env_vars, rcp_fpath) = self.prep_run_data(example)
        with helpers.with_test_wd():
            ninja.run_ninja(
                rcp_fpath, env_vars=env_vars, nr_tasks=1,
                add_run_args=self.silent_run_args)
        self.check_output_files(
            example, exp_ex_out_fpaths=None, exp_noex_out_fpaths=[])


    def test_base_dpath(self):
        '''
        Test using another base directory.
        '''
        # Standard test preparation.
        example = 'fibonacci'
        variant = 'rel'
        (env_vars, rcp_fpath) = self.prep_run_data(example, variant)
        with helpers.with_test_wd():
            ninja.run_ninja(
                rcp_fpath, env_vars=env_vars, base_dpath=self.test_base_dpath,
                add_run_args=self.silent_run_args)
        self.check_output_files(
            example, variant, exp_ex_out_fpaths=None, exp_noex_out_fpaths=[])


    def test_keep_going(self):
        '''
        Test with a build error but running Ninja keep-going.
        '''
        build_error = self.build_err_partial
        (env_vars, rcp_fpath) = self.prep_run_data(
            build_error=build_error)
        with self.assertRaises(ninja.NinjaCallError):
            with helpers.with_test_wd():
                ninja.run_ninja(
                    rcp_fpath, env_vars=env_vars, keep_going=True,
                    add_run_args=self.silent_run_args)
        self.check_output_files(build_error=build_error)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
