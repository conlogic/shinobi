# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`config` in the ``shinobi`` package.
'''


import pathlib
import sys
import stat

import duconfig

import shinobi.ninja as ninja
import shinobi.command as command
import shinobi.functions as functions

from . import helpers


# Classes
# =======


class base_command_TC(helpers.base_shinobi_TC):
    '''
    Base class for tests of the :py:mod:`command` module.
    '''
    def setUp(self):
        super().setUp()

        # Functions used for function call interpolation in the configuration
        # of the `fibonacci` example.
        self.fib_used_fcts = [
            functions.join_paths,
            functions.find_ext_in_dir,
            functions.file_base,
            functions.files_bases,
            functions.add_files_dir,
            functions.add_files_ext
        ]


    def prep_test_no_cfg(self, example=None, variant=None, build_error=None,
                         use_json=False, data_replaces=None,
                         dst_base_dpath=None):
        '''
        Prepare a test using source code, a Shinobi configuration file in CONF
        format (if `use_json` is ``True`` or JSON) format (if `use_json` is
        ``True``) in destination base directory `dst_base_dpath` either

        + from variant `variant` (or standard if `variant` is ``None``) of
          example `example` below the base test directory, or
        + for build error `build_error` with its example and variant, if
          `build_error` is not ``None``.

        For a ``None`` `data_replaces` the standard replacements are made
        during copying the Shinobi configuration file.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        :return: destination path of the copied configuration file, if there
                 is such an item, or ``None``s otherwise.
        '''
        cfg_fpath = self.cp_example_src_cfg(
            example, variant, build_error, use_json, data_replaces,
            dst_base_dpath, ensure_clean=True)

        return cfg_fpath


    def prep_test_with_cfg(self, example=None, variant=None, use_json=False,
                           cfg_args={}):
        '''
        Prepare a test using source code, a Shinobi configuration file in CONF
        format (if `use_json` is ``True`` or JSON) format (if `use_json` is
        ``True``) and a Ninja recipe from variant `variant` (or standard if
        `variant` is ``None``) of example `example` below the base test
        directory.

        A corresponding configuration object with the respective Shinobi
        configuration file read is prepared, too.  Dictionary `cfg_args` is
        used for the keyword arguments for the object's initialization instead
        of the default ones.

        :return: the configuration object created if there was a respective
                 Shinobi configuration file, or ``None`` otherwise.
        '''
        # Clean up the test base directory, and copy the example source files,
        # prepare a configuration file.
        cfg_fpath = self.prep_test_no_cfg(example, variant, use_json=use_json)

        # Prepare recipes.
        self.cp_example_recipe(example, variant)

        # Create an appropriate configuration object.
        cfg_obj = self.mk_cfg(cfg_fpath, cfg_args)

        return cfg_obj


class read_config_file_TC(base_command_TC):
    '''
    Tests for :py:func:`read_config_file`.
    '''


    def check_example_cfg_data(self, cfg, example, variant=None):
        '''
        Variant of :py:meth:`check_example_cfg_data` from the super class
        using always expanded configuration data.
        '''
        super().check_example_cfg_data(cfg, example, variant, raw=False)


    def test_config_error(self):
        '''
        Test with a configuration error.
        '''
        cfg_fpath = pathlib.Path('foo', 'bar')
        with self.assertRaises(duconfig.ConfigError):
            command.read_config_file(cfg_fpath)


    def test_default(self):
        '''
        Test using the defaults.
        '''
        example = 'guess_nat'
        cfg_fpath = self.prep_test_no_cfg(example)
        cfg = command.read_config_file(cfg_fpath)
        self.check_example_cfg_data(cfg, example)


    def test_oth_cfg_params(self):
        '''
        Test using other configuration parameters.
        '''
        example = 'fibonacci'
        variant = 'abs'
        cfg_fpath = self.prep_test_no_cfg(example, variant)
        # Use only the list of functions really needed for the configuration
        # file.
        cfg_params = {'fcts': self.fib_used_fcts}
        cfg = command.read_config_file(cfg_fpath, cfg_params=cfg_params)
        self.check_example_cfg_data(cfg, example, variant)


class run_shinobi_for_config_TC(base_command_TC):
    '''
    Tests for :py:func:`run_shinobi_for_config`.
    '''
    def setUp(self):
        super().setUp()

        # Standard functions for function call interpolation in
        # configurations.
        self.cfg_params_fcts = {'fcts': functions.CFG_FILE_FUNCTIONS}


    def test_config_error(self):
        '''
        Test with a configuration error.
        '''
        example = 'guess_nat'
        cfg = self.prep_test_with_cfg(example, cfg_args={})
        with helpers.with_test_wd(), self.assertRaises(duconfig.ConfigError):
            command.run_shinobi_for_config(
                cfg, add_run_args=self.silent_run_args)


    def test_noex_basedir(self):
        '''
        Test using a non-existing base directory.
        '''
        example = 'guess_nat'
        base_dpath = 'foo'
        cfg_fpath = self.prep_test_no_cfg(example)
        with self.assertRaises(ninja.NinjaCallError) as cm:
            command.run_shinobi_for_config(cfg_fpath, base_dpath=base_dpath)
        res_loc = cm.exception.loc
        self.assertEqual(str(res_loc), base_dpath)


    def test_default(self):
        '''
        Test using the defaults.
        '''
        example = 'guess_nat'
        cfg = self.prep_test_with_cfg(example, cfg_args=self.cfg_params_fcts)
        with helpers.with_test_wd():
            command.run_shinobi_for_config(
                cfg, add_run_args=self.silent_run_args)
        self.check_output_files(example)


    def test_base_dpath(self):
        '''
        Test using an explicit base directory path.
        '''
        example = 'fibonacci'
        variant = 'rel'
        cfg = self.prep_test_with_cfg(
            example, variant, cfg_args=self.cfg_params_fcts)
        command.run_shinobi_for_config(
            cfg, base_dpath=self.test_base_dpath,
            add_run_args=self.silent_run_args)
        self.check_output_files(example, variant)


class run_shinobi_TC(base_command_TC):
    '''
    Tests for :py:func:`run_shinobi`.
    '''


    def test_missg_ninja_syntax(self):
        '''
        Test with failing :py:mod:`ninja_syntax` import.
        '''
        example = 'guess_nat'
        cfg_fpath = self.prep_test_no_cfg(example)
        # Ensure that :py:mod:`ninja_syntax` is not loaded and cannot be
        # loaded.
        if 'ninja_syntax' in sys.modules.keys():
            del sys.modules['ninja_syntax']
        sys_path_bak = sys.path
        sys.path = []
        with self.assertRaises(ninja.NinjaRecipeError):
            command.run_shinobi(cfg_fpath)
        sys.path = sys_path_bak


    def test_not_writable_dir(self):
        '''
        Test using a not writable base directory.
        '''
        # Standard test preparation.
        example = 'fibonacci'
        variant = 'rel'
        base_dpath = self.test_base_dpath / 'foo'
        cfg_fpath = self.prep_test_no_cfg(
            example, variant, dst_base_dpath=base_dpath)
        # Make base directory not writable.
        base_dpath.chmod(stat.S_IRUSR | stat.S_IXUSR)
        try:
            with helpers.with_test_wd(base_dpath):
                with self.assertRaises(ninja.NinjaRecipeError):
                    command.run_shinobi(cfg_fpath)
        finally:
            # Make base directory writable again for deletion.
            base_dpath.chmod(stat.S_IRWXU)


    def test_noex_basedir(self):
        '''
        Test using a non-existing base directory.
        '''
        example = 'guess_nat'
        base_dpath = 'foo'
        cfg_fpath = self.prep_test_no_cfg(example)
        with self.assertRaises(ninja.NinjaCallError) as cm:
            command.run_shinobi(cfg_fpath, base_dpath=base_dpath)
        res_loc = cm.exception.loc
        self.assertEqual(str(res_loc), base_dpath)


    def test_ninja_cmd_error(self):
        '''
        Test with an erroneous Ninja command.
        '''
        example = 'fibonacci'
        variant = 'abs'
        ninja_cmd = 'foo'
        data_replaces = [
            (p, v) for (p, v) in self.data_replaces if not p == '<ninja_cmd>']
        data_replaces.append(('<ninja_cmd>', ninja_cmd))
        cfg_fpath = self.prep_test_no_cfg(
            example, variant, use_json=False, data_replaces=data_replaces)
        with helpers.with_test_wd():
            with self.assertRaises(ninja.NinjaCallError):
                command.run_shinobi(cfg_fpath)


    def test_build_error(self):
        '''
        Test with a build error.
        '''
        build_error = self.build_err_total
        variant = 'rel'
        cfg_fpath = self.prep_test_no_cfg(
            build_error=build_error, variant=variant)
        with helpers.with_test_wd():
            with self.assertRaises(ninja.NinjaCallError):
                command.run_shinobi(
                    cfg_fpath, add_run_args=self.silent_run_args)
        self.check_output_files(build_error=build_error, variant=variant)


    def test_conf_file(self):
        '''
        Test for a Shinobi CONF configuration file.
        '''
        example = 'guess_nat'
        cfg_fpath = self.prep_test_no_cfg(example, use_json=False)
        with helpers.with_test_wd():
            command.run_shinobi(cfg_fpath, add_run_args=self.silent_run_args)
        self.check_output_files(example)


    def test_json_file(self):
        '''
        Test for a Shinobi CONF configuration file.
        '''
        example = 'guess_nat'
        cfg_fpath = self.prep_test_no_cfg(example, use_json=True)
        with helpers.with_test_wd():
            command.run_shinobi(cfg_fpath, add_run_args=self.silent_run_args)
        self.check_output_files(example)


    def test_no_ninja_vars(self):
        '''
        Test using no Ninja variables.
        '''
        example = 'fibonacci'
        variant = 'nvr'
        cfg_fpath = self.prep_test_no_cfg(example, variant)
        with helpers.with_test_wd():
            command.run_shinobi(
                cfg_fpath, add_run_args=self.silent_run_args)
        self.check_output_files(example, variant)


    def test_oth_cfg_params(self):
        '''
        Test using other configuration parameters.
        '''
        example = 'fibonacci'
        variant = 'abs'
        cfg_fpath = self.prep_test_no_cfg(example, variant)
        # Use only the list of functions really needed for the configuration
        # file.
        cfg_params = {'fcts': self.fib_used_fcts}
        with helpers.with_test_wd():
            command.run_shinobi(
                cfg_fpath, add_run_args=self.silent_run_args,
                cfg_params=cfg_params)
        self.check_output_files(example, variant)


    def test_oth_ninja_cmd(self):
        '''
        Test using an alternative Ninja command.
        '''
        # We will use a fake Ninja command.
        ninja_cmd_fpath = self.test_base_dpath / 'fake_ninja.py'
        ninja_cmd = f"python3 {ninja_cmd_fpath}"
        # Standard test preparation.
        example = 'fibonacci'
        variant = 'abs'
        data_replaces = [
            (p, v) for (p, v) in self.data_replaces if not p == '<ninja_cmd>']
        data_replaces.append(('<ninja_cmd>', ninja_cmd))
        cfg_fpath = self.prep_test_no_cfg(
            example, variant, data_replaces=data_replaces)
        # Add a Ninja call for testing.
        my_fpath = self.test_base_dpath / 'foobar'
        with open(ninja_cmd_fpath, 'wt') as ninja_cmd_fobj:
            ninja_cmd_fobj.write('from shutil import copy\n')
            ninja_cmd_fobj.write(f"copy('{ninja_cmd_fpath}', '{my_fpath}')\n")
        # Call Ninja, and check result.
        with helpers.with_test_wd():
            command.run_shinobi(cfg_fpath)
        self.check_output_files(
            example, variant,
            exp_ex_out_fpaths=[my_fpath], exp_noex_out_fpaths=None)


    def test_base_dpath(self):
        '''
        Test using an explicit base directory path.
        '''
        example = 'fibonacci'
        variant = 'rel'
        cfg_fpath = self.prep_test_no_cfg(example, variant)
        command.run_shinobi(
            cfg_fpath, add_run_args=self.silent_run_args,
            base_dpath=self.test_base_dpath)
        self.check_output_files(example, variant)


    def test_dry_run(self):
        '''
        Test for a dry run.
        '''
        example = 'guess_nat'
        cfg_fpath = self.prep_test_no_cfg(example)
        with helpers.with_test_wd():
            command.run_shinobi(
                cfg_fpath, add_run_args=self.silent_run_args,
                dry_run=True)
        self.check_output_files(
            example, exp_ex_out_fpaths=[], exp_noex_out_fpaths=None)


    def test_nr_tasks(self):
        '''
        Test with a given number of parallel tasks.
        '''
        example = 'guess_nat'
        cfg_fpath = self.prep_test_no_cfg(example)
        with helpers.with_test_wd():
            command.run_shinobi(
                cfg_fpath, add_run_args=self.silent_run_args,
                nr_tasks=1)
        self.check_output_files(
            example, exp_ex_out_fpaths=None, exp_noex_out_fpaths=[])


    def test_keep_going(self):
        '''
        Test with a build error but running Ninja keep-going.
        '''
        build_error = self.build_err_partial
        cfg_fpath = self.prep_test_no_cfg(build_error=build_error)
        with helpers.with_test_wd():
            with self.assertRaises(ninja.NinjaCallError):
                command.run_shinobi(
                    cfg_fpath, add_run_args=self.silent_run_args,
                    keep_going=True)
        self.check_output_files(build_error=build_error)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
