# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-10-20
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers for tests.
'''

import pathlib
import tempfile
import shutil
import os
import json
import re
import difflib
import unittest
import subprocess

import shinobi.config as config
import shinobi.utilities as utilities


# Functions and classes
# =====================


def ensure_clean_dir(dpath):
    '''
    Ensure that directory with path `dpath` exists and is empty.
    '''
    dpath = pathlib.Path(dpath)

    if dpath.is_dir():
        shutil.rmtree(dpath)

    dpath.mkdir(parents=True)


def replace_in_file(fpath, old, new, out_fpath=None):
    '''
    Replace in file with path `fpath` all occurrences of string `old` by
    string `new`.

    For a not-``None`` path `out_fpath` result is written to a file with this
    path instead of the original one.
    '''
    with open(fpath, 'rt') as fobj:
        lines = fobj.readlines()

    if out_fpath is None:
        out_fpath = fpath
    with open(out_fpath, 'wt') as out_fobj:
        for l in lines:
            l_out = l.replace(old, new)
            out_fobj.write(l_out)


def files_in_subdir(base_dpath, sdname, mk_relative=False):
    '''
    Find all files residing in a subdirectory with name `sdname` below base
    directory with path `base_dpath`.

    For a ``True`` switch `mk_relative` the paths of the so found files are
    made relative to the base directory.

    :return: a list of paths of the found files.
    '''
    fpaths_found = []
    for (d, ds, fs) in os.walk(base_dpath):
        if sdname in ds:
            dpath_d = pathlib.Path(d) / sdname
            for p in dpath_d.iterdir():
                if p.is_file():
                    if mk_relative:
                        used_p = p.relative_to(base_dpath)
                    else:
                        used_p = p
                    fpaths_found.append(used_p)

    return fpaths_found


def read_json_file(fpath):
    '''
    :return: content of JSON file with path `fpath` read into a dictionary.
    '''
    with open(fpath, 'rt') as fobj:
        content = json.load(fobj)

    return content


def write_json_file(content, fpath, indent=1):
    '''
    Write content `content` to JSON file with path `fpath` using indentation
    level `indent`.
    '''
    fpath = pathlib.Path(fpath)
    dpath = fpath.parent
    dpath.mkdir(parents=True, exist_ok=True)
    with open(fpath, 'wt') as fobj:
        json.dump(content, fobj, indent=indent)


class base_shinobi_TC(unittest.TestCase):
    '''
    Base class for tests for the ``shinobi`` package.
    '''
    def assertFileEqual(self, fpath_x, fpath_y, msg=None):
        '''
        Assert that the content of the files with paths `fpath_x` and
        `fpath_y` is equal.

        A not-``None`` string `msg` is used as alternative message instead of
        the standard one if the assertion fails.

        :raise: a :py:exc:`failureException` if the assertion fails.
        '''
        fpath_x = pathlib.Path(fpath_x)
        fpath_y = pathlib.Path(fpath_y)

        raise_exp = False
        std_msg = ""

        with open(fpath_x, 'rt') as fobj_x:
            lines_x = fobj_x.readlines()
        with open(fpath_y, 'rt') as fobj_y:
            lines_y = fobj_y.readlines()
        diff_iter_x_y = difflib.unified_diff(
            lines_x, lines_y, str(fpath_x), str(fpath_y))
        diff_lines_x_y = list(diff_iter_x_y)

        if not diff_lines_x_y == []:
            std_msg = f"Files differ:\n{''.join(diff_lines_x_y)}"
            raise_exp = True

        if raise_exp:
            msg = self._formatMessage(msg, std_msg)
            raise self.failureException(msg)


    def assertFileExist(self, fpath, msg=None):
        '''
        Assert that file with path `fpath` exists.

        A not-``None`` string `msg` is used as alternative message instead of
        the standard one if the assertion fails.

        :raise: a :py:exc:`failureException` if the assertion fails.
        '''
        fpath = pathlib.Path(fpath)
        if not fpath.is_file():
            std_msg = f"File does not exist:\n{fpath}"
            msg = self._formatMessage(msg, std_msg)
            raise self.failureException(msg)


    def assertFileNotExist(self, fpath, msg=None):
        '''
        Assert that file with path `fpath` does not exist.

        A not-``None`` string `msg` is used as alternative message instead of
        the standard one if the assertion fails.

        :raise: a :py:exc:`failureException` if the assertion fails.
        '''
        fpath = pathlib.Path(fpath)
        if fpath.is_file():
            std_msg = f"File does exist:\n{fpath}"
            msg = self._formatMessage(msg, std_msg)
            raise self.failureException(msg)


    def assertSubstringIn(self, str_x, str_y, msg=None):
        '''
        Assert that string `str_x` is a substring of string `str_y`.

        A not-``None`` string `msg` is used as alternative message instead of
        the standard one if the assertion fails.

        :raise: a :py:exc:`failureException` if the assertion fails.
        '''
        if str_y.find(str_x) == -1:
            std_msg = f"String '{str_x}' is no substring of '{str_y}'"
            msg = self._formatMessage(msg, std_msg)
            raise self.failureException(msg)


    def assertMinLinesNr(self, s, nr, msg=None):
        '''
        Assert that the string `s` has at least `nr` many lines.

        A not-``None`` string `msg` is used as alternative message instead of
        the standard one if the assertion fails.

        :raise: a :py:exc:`failureException` if the assertion fails.
        '''
        lines = s.split('\n')
        nr_lines = len(lines)
        if nr_lines < nr:
            std_msg = f"String has {nr_lines} lines instead of >= {nr}."
            msg = self._formatMessage(msg, std_msg)
            raise self.failureException(msg)


    def setUp(self):
        # Directory path for the test modules.
        self.tests_dpath = pathlib.Path(__file__).parent
        # Path for the directory containing the examples.
        self.examples_dpath = self.tests_dpath.parent / 'examples'
        # Path for the base test directory.
        self.test_base_dpath = \
            pathlib.Path(tempfile.gettempdir()) / 'shinobi-tests'
        # Paths for the directory with test data.
        self.test_data_dpath = self.tests_dpath / 'data'
        # Separator for variants.
        self.varsep = '_'
        # Name of subdirectory in examples containing the source code.
        self.src_sdname = 'src'
        # Name of subdirectory in examples containing the generated files.
        self.bin_sdname = 'bin'
        # Standard filename for Shinobi configuration files.
        # - Base.
        self.cfg_fbase = 'shinobi'
        # - CONF format.
        self.cfg_conf_fname = self.cfg_fbase + os.extsep + 'conf'
        # - JSON format.
        self.cfg_json_fname = self.cfg_fbase + os.extsep + 'json'
        # Standard filename for Ninja recipes.
        self.rcp_fname = 'build.ninja'
        # Subdirectory for expected test data.
        self.exp_sdpath = 'expected'

        # Standard replacements to do when preparing test data files.
        self.data_replaces = [
            ('<basedir>', str(self.test_base_dpath)),
            ('<ninja_cmd>', 'ninja')
        ]
        # Additional arguments to run Ninja silently.
        self.silent_run_args = {
            'stdout': subprocess.PIPE, 'stderr': subprocess.PIPE
        }

        # Data for a build errors.
        # - Example ``guess_nat``, partial success when keep going.
        self.build_err_partial = {
            'example': 'guess_nat',
            'src_fpath': pathlib.Path('players', 'computer', 'player.c'),
            'old_str': 'switch',
            'new_str': 'foo',
            'exp_ex_fpaths': [
                pathlib.Path('bin', 'guess_interact')
            ]
        }
        # - Example ``fibonacci``, total failure.
        self.build_err_total = {
            'example': 'fibonacci',
            'src_fpath': pathlib.Path('c', 'fibonacci.c'),
            'old_str': 'printf',
            'new_str': 'foo',
            'exp_noex_fpaths': [
                pathlib.Path('bin', 'fibonacci')
            ]
        }

        # Bases for some test data filenames.
        # - Data of configuration as nested dictionary.
        self.cfg_nest_fbase = 'cfg_nest'
        # - Data of configuration used to generate Ninja recipes.
        self.rcp_fbase = 'recipe'
        # - Paths of generated files by running Ninja.
        self.out_fbase = 'out_files'


    def _tokenize_name(self, name):
        '''
        Tokenize name `name` to insert a variant.

        :return: pair of `name` parts in front and in back of a variant.
        '''
        fullext = ''.join(pathlib.Path(name).suffixes)
        if len(fullext) > 0:
            name_beg = name[:-len(fullext)] + self.varsep
        else:
            name_beg = name + self.varsep
        name_end = fullext

        return (name_beg, name_end)


    def _all_variants(self, std_name, example):
        '''
        Calculate all variants for file or directory with standard name
        `std_name` for example `example`.

        :return: list of all variants found, including ``None`` if there is a
                 standard variant.
        '''
        (name_beg, name_end) = self._tokenize_name(std_name)
        name_rx = re.escape(name_beg) + '(.*)' + re.escape(name_end) + '$'
        example_dpath = self.examples_dpath / example
        name_vars = []
        for p in example_dpath.iterdir():
            name_p = p.name
            if name_p == std_name:
                name_vars.append(None)
            else:
                matcher_p = re.match(name_rx, name_p)
                if matcher_p is not None:
                    var_p = matcher_p.group(1)
                    name_vars.append(var_p)

        return name_vars


    def _variant_name(self, std_name, variant=None):
        '''
        :return: name of file or directory with standard name `std_name` from
                 variant `variant` (or standard if `variant` is ``None``).
        '''
        if variant is not None:
            (beg_name, end_name) = self._tokenize_name(std_name)
            var_name = beg_name + variant + end_name
        else:
            var_name = std_name

        return var_name


    def _example_path(self, std_name, example, variant=None):
        '''
        :return: path for file or directory with standard name `std_name` from
                 variant `variant` (or standard if `variant` is ``None``) of
                 example `example`.
        '''
        example_path = self.examples_dpath / example
        var_name = self._variant_name(std_name, variant)
        example_var_path = example_path / var_name

        return example_var_path


    def _test_data_fpath(self, std_name, example, variant=None, raw=True):
        '''
        return: path for raw (if `raw` is ``True``) or expanded (if `raw`
                is ``False``) test data with standard file name or base
                `std_name` from variant `variant` (or standard if `variant` is
                ``None``) of example `example`.
        '''
        std_fbase = pathlib.Path(std_name).with_suffix('').name
        datatype_str = 'raw' if raw else 'epd'
        datatype_fbase = std_fbase + self.varsep + datatype_str
        datatype_fname = datatype_fbase + os.extsep + 'json'
        var_fname = self._variant_name(datatype_fname, variant)
        data_fpath = self.test_data_dpath / example / var_fname

        return data_fpath


    def _cp_example_path(self, std_name, example, variant=None,
                         content_only=False, dst_base_dpath=None,
                         ensure_clean=False):
        '''
        Copy file or directory with standard name `std_name` from variant
        `variant` (or standard if `variant` is ``None``) of example `example`
        below the destination base directory `dst_base_dpath`.

        In case of a directory, for a ``True`` switch `content_only` only the
        content of the directory instead of the directory itself is copied.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        For a ``True`` switch `ensure_clean` a clean destination base
        directory is ensured before copying.

        :return: destination path of the copied file or directory, if there is
                 such an item to be copied, or ``None``s otherwise.
        '''
        if dst_base_dpath is None:
            dst_base_dpath = self.test_base_dpath
        else:
            dst_base_dpath = pathlib.Path(dst_base_dpath)

        # Ensure clean destination base directory, if wanted.
        if ensure_clean:
            ensure_clean_dir(dst_base_dpath)

        # Determine source path.
        src_path = self._example_path(std_name, example, variant)

        # Copy example stuff, if it exists.
        if src_path.is_file():
            dst_path = dst_base_dpath / std_name
            dst_base_dpath.mkdir(parents=True, exist_ok=True)
            shutil.copy(src_path, dst_path)
        elif src_path.is_dir():
            if content_only:
                dst_path = dst_base_dpath
            else:
                dst_path = dst_base_dpath / std_name
            dst_base_dpath.mkdir(parents=True, exist_ok=True)
            shutil.copytree(src_path, dst_path, dirs_exist_ok=True)
        else:
            dst_path = None

        return dst_path


    def cp_example_src(self, example=None, build_error=None,
                       dst_base_dpath=None, ensure_clean=False):
        '''
        Copy source code below the destination base directory `dst_base_dpath`
        either

        + from example `example` below the base test directory, or
        + for build error `build_error` with its example, if `build_error` is
          not ``None``.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        For a ``True`` switch `ensure_clean` a clean destination base
        directory is ensured before copying.

        :return: the example used.
        '''
        # Determine example, if a build error is given.
        if build_error is not None:
            example = build_error['example']

        # Copy source files.
        dst_src_dpath = self._cp_example_path(
            self.src_sdname, example, variant=None, content_only=True,
            dst_base_dpath=dst_base_dpath, ensure_clean=ensure_clean)

        # For a build error, modify source files.
        if build_error is not None:
            err_src_fpath = dst_src_dpath / build_error['src_fpath']
            replace_in_file(
                err_src_fpath, build_error['old_str'], build_error['new_str'])

        return example


    def cp_example_cfg(self, example, variant=None, use_json=False,
                       data_replaces=None, dst_base_dpath=None):
        '''
        Copy the Shinobi configuration file in CONF format (if `use_json` is
        ``False``) or JSON format (if `use_json` is ``True``) from variant
        `variant` (or standard if `variant` is ``None``) of example `example`
        below destination base directory `dst_base_dpath`.

        For a ``None`` `data_replaces` the standard replacements are made
        during copying the Shinobi configuration file.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        For a ``True`` switch `ensure_clean` a clean destination base
        directory is ensured before copying.

        :return: destination path of the copied configuration file, if there
                 is such a file, or ``None``s otherwise.
        '''
        if data_replaces is None:
            data_replaces = self.data_replaces

        cfg_fname = self.cfg_json_fname if use_json else self.cfg_conf_fname
        dst_fpath = self._cp_example_path(
            cfg_fname, example, variant, dst_base_dpath=dst_base_dpath)
        for (p, v) in data_replaces:
            replace_in_file(dst_fpath, p, v)

        return dst_fpath


    def cp_example_src_cfg(self, example=None, variant=None, build_error=None,
                           use_json=None, data_replaces=None,
                           dst_base_dpath=None, ensure_clean=False):
        '''
        Copy source code either:

        + from example `example` below the base test directory, or
        + for build error `build_error` with its example, if `build_error` is
          not ``None``.

        and a Shinobi configuration file in CONF format (if `use_json` is
        ``True`` or JSON) format (if `use_json` is ``True``); both below the
        destination base directory `dst_base_dpath`.

        For a ``None`` `data_replaces` the standard replacements are made
        during copying the Shinobi configuration file.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        For a ``True`` switch `ensure_clean` a clean destination base
        directory is ensured before copying.

        :return: destination path of the copied configuration file, if there
                 is such an item, or ``None``s otherwise.
        '''
        # Clean up the test base directory, copy sources, prepare a build
        # error, if wanted.
        example = self.cp_example_src(
            example, build_error, dst_base_dpath, ensure_clean)

        # Prepare configuration file.
        cfg_fpath = self.cp_example_cfg(
            example, variant, use_json, data_replaces, dst_base_dpath)

        return cfg_fpath


    def mk_cfg(self, cfg_fpath, cfg_args={}):
        '''
        Safely make a configuration object for the Shinobi configuration file
        with path `cfg_fpath`.  If `cfg_fpath` is ``None``, nothing is done.

        Dictionary `cfg_args` is used for the keyword arguments for the
        object's initialization.

        :return: the configuration object created if there was a respective
                 Shinobi configuration file, or ``None`` otherwise.
        '''
        if cfg_fpath is not None:
            cfg_obj = config.Config(**cfg_args)
            cfg_obj.read_file(cfg_fpath)
        else:
            cfg_obj = None

        return cfg_obj


    def cp_example_recipe(self, example, variant=None, dst_base_dpath=None):
        '''
        Copy the example Ninja recipe from variant `variant` (or standard if
        `variant` is ``None``) of example `example` below test base directory
        `dst_base_dpath`.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        :return: path of the destination directory for the copied recipe.
        '''
        dst_fpath = self._cp_example_path(
            self.rcp_fname, example, variant, dst_base_dpath=dst_base_dpath)
        for (p, v) in self.data_replaces:
            replace_in_file(dst_fpath, p, v)

        return dst_fpath


    def check_example_cfg_data(self, cfg, example, variant=None, raw=True):
        '''
        Check the raw (if `raw` is ``True``) or expanded (if `raw` is
        ``False``) configuration data of :py:class:`Config` object `cfg`
        against the configuration test data from variant `variant` (or
        standard if `variant` is ``None``) of example `example`.

        :raise: a :py:exc:`failureException` if the test fails.
        '''
        with with_test_wd():
            res_cfg_data = dict(cfg)

        exp_cfg_fpath = self._test_data_fpath(
            self.cfg_nest_fbase, example, variant, raw)
        exp_cfg_data = read_json_file(exp_cfg_fpath)

        self.assertDictEqual(res_cfg_data, exp_cfg_data)


    def check_recipe_file(self, rcp_fpath, example, variant=None,
                          header=None):
        '''
        Check Ninja recipe file with path `rcp_fpath` by comparing it with the
        Ninja recipe file from variant `variant` (or standard if `variant` is
        ``None``) of example `example`.

        A not-``None`` string `header` is used for an alternative header in
        the expected Ninja recipe file.

        :raise: a :py:exc:`failureException` if the test fails.
        '''
        # Copy the example Ninja recipe file.
        exp_dst_dpath = self.test_base_dpath / self.exp_sdpath
        exp_rcp_fpath = self.cp_example_recipe(
            example, variant, exp_dst_dpath)

        # Change the header in the expected Ninja recipe file, if wanted.
        if header is not None:
            with open(exp_rcp_fpath, 'rt') as exp_fobj:
                old_header = exp_fobj.readline().rstrip()
            new_header = f"# {header}"
            replace_in_file(exp_rcp_fpath, old_header, new_header)

        # Compare the Ninja recipe files.
        self.assertFileEqual(rcp_fpath, exp_rcp_fpath)


    def check_output_files(self, example=None, variant=None, build_error=None,
                           exp_ex_out_fpaths=None, exp_noex_out_fpaths=[],
                           base_dpath=None):
        '''
        Check (non-)existence of output files after a run of Ninja: all output
        files with paths in list `exp_ex_out_fpaths` must exist, but those
        files with paths in list `exp_noex_out_fpaths` must not exist.

        If one of `exp_ex_out_fpaths` or `exp_noex_out_fpaths` is ``None``,
        the list of paths is taken from the output data file from variant
        `variant` (or standard if `variant` is ``None``) of example `example`,
        if `example` is not ``None``, or from build error information
        `build_error`, if `build_error` is not ``None``.

        For a not-``None`` path `base_dpath` the directory with this path is
        used as base directory instead of the test base directory.

        :raise: a :py:exc:`failureException` if the test fails.
        '''
        # Calculate needed output file paths from test data, if not given.
        if exp_ex_out_fpaths is not None:
            # Explicitly given paths -> use them.
            pass
        elif example is not None:
            # Example given -> use its expected output files.
            out_fpath = self._test_data_fpath(
                self.out_fbase, example, variant)
            exp_ex_out_fpaths = read_json_file(out_fpath)
        elif build_error is not None:
            # Build error given -> use its files expected to exist, if given.
            exp_ex_out_fpaths = build_error.get('exp_ex_fpaths', [])
        else:
            exp_ex_out_fpaths = []
        if exp_noex_out_fpaths is not None:
            # Explicitly given paths -> use them.
            pass
        elif example is not None:
            # Example given -> use its expected output files as expected not
            # to exist.
            out_fpath = self._test_data_fpath(
                self.out_fbase, example, variant)
            exp_noex_out_fpaths = read_json_file(out_fpath)
        elif build_error is not None:
            # Build error given -> use its files expected not to exist, if
            # given.
            exp_noex_out_fpaths = build_error.get('exp_noex_fpaths', [])
        else:
            exp_ex_out_fpaths = []

        # Check (non-)existence for the output files.
        if base_dpath is None:
            base_dpath = self.test_base_dpath
        cwd_dpath = pathlib.Path.cwd()
        try:
            os.chdir(base_dpath)
            for f in exp_ex_out_fpaths:
                if f not in exp_noex_out_fpaths:
                    self.assertFileExist(f)
            for f in exp_noex_out_fpaths:
                self.assertFileNotExist(f)
        finally:
            os.chdir(cwd_dpath)


    def mk_example_test_data(self, example):
        '''
        Make test data for example `example`.

        :return: list of paths of the written test data files.
        '''
        all_vars = self._all_variants(self.cfg_conf_fname, example)
        data_fpaths = []
        for v in all_vars:
            data_fpaths_v = self.mk_example_variant_test_data(example, v)
            data_fpaths.extend(data_fpaths_v)

        return data_fpaths


    def mk_test_data(self):
        '''
        Make test data for all examples.

        :return: list of paths of the written test data files.
        '''
        data_dpaths = []
        for p in self.examples_dpath.iterdir():
            sample_p = p.name
            data_dpaths_p = self.mk_example_test_data(sample_p)
            data_dpaths.extend(data_dpaths_p)

        return data_dpaths


class with_test_wd(utilities.with_wd):
    '''
    Context manager to use an alternative working directory for the test.
    '''
    def __init__(self, work_dpath=None):
        '''
        Initialize a context to use working directory with path `work_dpath`.

        If `work_dpath` is ``None``, the path of the default test base
        directory is used.
        '''
        if work_dpath is None:
            tc = base_shinobi_TC()
            tc.setUp()
            work_dpath = tc.test_base_dpath

        super().__init__(work_dpath)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
