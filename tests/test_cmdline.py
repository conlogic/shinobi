# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-10-14
#
# Description    : See documentation string below.
# ============================================================================

'''
Unit tests for module :py:mod:`cmdline` in the ``shinobi`` package.
'''


import contextlib
import io
import pathlib
import shutil
import stat

import shinobi.utilities as utilities
import shinobi.cmdline as cmdline
from shinobi import version as shinobi_vers

from . import helpers


# Classes
# =======


class calc_config_fpath_TC(helpers.base_shinobi_TC):
    '''
    Tests for the :py:func:`calc_config_fpath` function.
    '''
    def setUp(self):
        super().setUp()

        if self.test_base_dpath.is_dir():
            shutil.rmtree(self.test_base_dpath)


    def test_missg_cfg_file(self):
        '''
        Test with a missing configuration file.
        '''
        with self.assertRaises(cmdline.CommandlineError):
            cmdline.calc_config_fpath(
                None, self.cfg_fbase, self.test_base_dpath)


    def test_std_conf_cfg_file(self):
        '''
        Test for a CONF configuration file with standard filename.
        '''
        example = 'guess_nat'
        cfg_fpath = self.cp_example_cfg(example, use_json=False)
        res_cfg_fpath = cmdline.calc_config_fpath(
            None, self.cfg_fbase, self.test_base_dpath)
        self.assertEqual(res_cfg_fpath, cfg_fpath)


    def test_std_json_cfg_file(self):
        '''
        Test for a JSON configuration file with standard filename.
        '''
        example = 'guess_nat'
        cfg_fpath = self.cp_example_cfg(example, use_json=True)
        res_cfg_fpath = cmdline.calc_config_fpath(
            None, self.cfg_fbase, self.test_base_dpath)
        self.assertEqual(res_cfg_fpath, cfg_fpath)


    def test_std_conf_json_cfg_file(self):
        '''
        Test for both a CONF configuration file and a JSON configuration file
        with standard filenames.
        '''
        example = 'guess_nat'
        conf_cfg_fpath = self.cp_example_cfg(example, use_json=False)
        self.cp_example_cfg(example, use_json=True)
        res_cfg_fpath = cmdline.calc_config_fpath(
            None, self.cfg_fbase, self.test_base_dpath)
        self.assertEqual(res_cfg_fpath, conf_cfg_fpath)


    def test_alt_abs_cfg_fpath(self):
        '''
        Test with an alternative absolute path for the configuration file.
        '''
        example = 'guess_nat'
        abs_cfg_fpath = self.cp_example_cfg(example)
        res_cfg_fpath = cmdline.calc_config_fpath(
            abs_cfg_fpath, self.cfg_fbase, self.test_base_dpath)
        self.assertEqual(res_cfg_fpath, abs_cfg_fpath)


    def test_alt_rel_cfg_fpath(self):
        '''
        Test with an alternative relative path for the configuration file.
        '''
        example = 'guess_nat'
        abs_cfg_fpath = self.cp_example_cfg(example)
        rel_cfg_fpath = abs_cfg_fpath.relative_to(self.test_base_dpath)
        res_cfg_fpath = cmdline.calc_config_fpath(
            rel_cfg_fpath, self.cfg_fbase, self.test_base_dpath)
        self.assertEqual(res_cfg_fpath, abs_cfg_fpath)


class get_stdout(contextlib.redirect_stdout):
    '''
    Context manager to catch output to STDOUT and to store it in an
    attribute.
    '''
    def __init__(self):
        '''
        Initialize context manager of this type.
        '''
        # An IO string to catch and store STDOUT output.
        self._stdout_catcher = io.StringIO()
        # Initialize a :py:class:`redirect_stdout` context manager using our
        # IO string.
        super().__init__(self._stdout_catcher)

        # To store caught STDOUT output when leaving this context.
        self.out = None


    def __enter__(self):
        '''
        Enter a context of this type.
        '''
        # We need for ``with get_stdout as ...`` an own instance and not one
        # of the super class.
        super().__enter__()

        return self


    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Leave a context of this type, where for not-``None`` type `exc_type`
        is the type of raised exception, a not-``None`` `exc_obj` is the
        exception raised, and a not-``None`` `traceback` is the associated
        trace back.
        '''
        # First we leave the redirect context.
        super().__exit__(exc_type, exc_obj, traceback)

        # Finally we store the caught STDOUT output in an attribute.
        self.out = self._stdout_catcher.getvalue().rstrip()


class base_main_TC(helpers.base_shinobi_TC):
    '''
    Base class of :py:func:`main` tests.
    '''
    def setUp(self):
        super().setUp()

        # Name and path of the command script.
        self.cmd_name = 'snob'
        self.cmd_fpath = self.tests_dpath / self.cmd_name


    def tearDown(self):
        # To prevent that tests for logging interfere with other tests, all
        # logging is always disabled at the end of a test.
        utilities.set_max_loglevel(0)
        utilities.disable_debug_log()


    def prep_argv(self, options):
        '''
        Prepare a list of commandline arguments (aka ``sys.argv``) using
        options form list `options`.

        :return: prepared commandline arguments list.
        '''
        argv = [self.cmd_fpath]
        argv.extend(options)

        return argv


class get_stdout_sysexit(get_stdout):
    '''
    Context manager to catch output to STDOUT and to store it in an
    attribute, plus to catch an :py:exc:`SystemExit` exception and store its
    return code to store it in another attribute,
    '''
    def __init__(self):
        '''
        Initialize context manager of this type.
        '''
        super().__init__()

        # To store the return code of caught :py:exc:`SystemExit`.
        self.rc = None


    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Leave a context of this type, where for not-``None`` type `exc_type`
        is the type of raised exception, a not-``None`` `exc_obj` is the
        exception raised, and a not-``None`` `traceback` is the associated
        trace back.
        '''
        super().__exit__(exc_type, exc_obj, traceback)

        # Handle a :py:exc:`SystemExit`.
        if exc_type is SystemExit:
            self.rc = exc_obj.code

            return True


class main_no_cfg_TC(base_main_TC):
    '''
    Tests for :py:func:`main` that do not need a Shinobi configuration file.
    '''
    @staticmethod
    def exp_usage_begin(prog_name):
        '''
        :return: begin of the usage message string for program name
                 `prog_name`.
        '''
        cmdline_parser = cmdline.ArgumentParser()
        usage_str = cmdline_parser.format_usage()
        usage_prefix = usage_str.split(' ')[0]
        usage_begin = usage_prefix + ' ' + prog_name

        return usage_begin


    def setUp(self):
        super().setUp()

        # Template for help version to STDOUT.
        self.vers_out_tpl = f"{self.cmd_name} {{}}"


    def test_h(self):
        '''
        Test option ``-h``.
        '''
        argv = self.prep_argv(['-h'])
        with get_stdout_sysexit() as cm:
            cmdline.main(argv)
        exp_usage_out = self.exp_usage_begin(self.cmd_name)
        self.assertSubstringIn(exp_usage_out, cm.out)
        self.assertEqual(cm.rc, 0)


    def test_help(self):
        '''
        Test option ``--help``.
        '''
        argv = self.prep_argv(['--help'])
        with get_stdout_sysexit() as cm:
            cmdline.main(argv)
        exp_usage_out = self.exp_usage_begin(self.cmd_name)
        self.assertSubstringIn(exp_usage_out, cm.out)
        self.assertEqual(cm.rc, 0)


    def test_V(self):
        '''
        Test option ``-V``.
        '''
        argv = self.prep_argv(['-V'])
        with get_stdout_sysexit() as cm:
            cmdline.main(argv)
        exp_usage_out = self.vers_out_tpl.format(shinobi_vers)
        self.assertSubstringIn(exp_usage_out, cm.out)
        self.assertEqual(cm.rc, 0)


    def test_version(self):
        '''
        Test option ``--version``.
        '''
        argv = self.prep_argv(['--version'])
        with get_stdout_sysexit() as cm:
            cmdline.main(argv)
        exp_usage_out = self.vers_out_tpl.format(shinobi_vers)
        self.assertSubstringIn(exp_usage_out, cm.out)
        self.assertEqual(cm.rc, 0)


    def test_oth_tool(self):
        '''
        Test using another tool name.
        '''
        tool = 'foobar'
        argv = self.prep_argv(['-h'])
        with get_stdout_sysexit() as cm:
            cmdline.main(argv, tool=tool)
        exp_usage_out = f" {tool} "
        self.assertSubstringIn(exp_usage_out, cm.out)
        self.assertEqual(cm.rc, 0)


    def test_oth_version(self):
        '''
        Test using another version.
        '''
        version = 'foo.bar'
        argv = self.prep_argv(['-V'])
        with get_stdout_sysexit() as cm:
            cmdline.main(argv, version=version)
        exp_usage_out = self.vers_out_tpl.format(version)
        self.assertSubstringIn(exp_usage_out, cm.out)
        self.assertEqual(cm.rc, 0)


class get_stdout_suppress_stderr(get_stdout):
    '''
    Context manager to catch output to STDOUT and to store it in an
    attribute, plus to suppress STDERR output.
    '''
    def __init__(self):
        '''
        Initialize context manager of this type.
        '''
        # Another IO string is needed for a :py:class:`redirect_stderr`
        # context manager to catch STDERR output.
        stderr_catcher = io.StringIO()
        self._stderr_cm = contextlib.redirect_stderr(stderr_catcher)

        super().__init__()


    def __enter__(self):
        '''
        Enter a context of this type.
        '''
        # We first redirect STDERR.
        self._stderr_cm.__enter__()

        return super().__enter__()


    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Leave a context of this type, where for not-``None`` type `exc_type`
        is the type of raised exception, a not-``None`` `exc_obj` is the
        exception raised, and a not-``None`` `traceback` is the associated
        trace back.
        '''
        super().__exit__(exc_type, exc_obj, traceback)

        # We leave the STDERR redirect context last.
        self._stderr_cm.__exit__(exc_type, exc_obj, traceback)


class main_with_cfg_TC(base_main_TC):
    '''
    Tests for :py:func:`main` that do not need a Shinobi configuration file.
    '''


    def prep_test(self, options, example=None, variant=None, build_error=None,
                  use_json=False, dst_base_dpath=None):
        '''
        Prepare a test using options list `options`, source code, a Shinobi
        configuration file in CONF format (if `use_json` is ``True`` or JSON)
        format (if `use_json` is ``True``) in destination base directory
        `dst_base_dpath` either

        + from variant `variant` (or standard if `variant` is ``None``) of
          example `example` below the base test directory, or
        + for build error `build_error` with its example and variant, if
          `build_error` is not ``None``.

        For a ``None`` path `dst_base_dpath` the test base directory is used
        as destination base directory.

        :return: a pair with the list of commandline arguments for
        :py:func:`main`, and the path of the prepaired Shinobi configuration
        file.
        '''
        cfg_fpath = self.cp_example_src_cfg(
            example, variant, build_error, use_json,
            dst_base_dpath=dst_base_dpath, ensure_clean=True)

        argv = self.prep_argv(options)

        return (argv, cfg_fpath)


    def test_noex_opt(self):
        '''
        Test for unknown option.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test(['--foo'], example, use_json=False)
        with get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.assertEqual(rc, 1)


    def test_missg_cfg_file(self):
        '''
        Test for missing Shinobi configuration file path at all.
        '''
        example = 'guess_nat'
        (argv, cfg_fpath) = self.prep_test([], example, use_json=False)
        cfg_fpath.unlink()
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.assertEqual(rc, 1)


    def test_noex_cfg_file(self):
        '''
        Test for a path for a not existing Shinobi configuration file.
        '''
        argv = self.prep_argv(['-c', str(pathlib.Path('foo', 'bar'))])
        with get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.assertEqual(rc, 2)


    def test_not_writable_dir(self):
        '''
        Test using a not writable base directory.
        '''
        # Standard test preparation.
        example = 'fibonacci'
        variant = 'rel'
        base_dpath = self.test_base_dpath / 'foo'
        (argv, _) = self.prep_test(
            ['-b', str(base_dpath)], example, variant,
            dst_base_dpath=base_dpath)
        # Make base directory not writable.
        base_dpath.chmod(stat.S_IRUSR | stat.S_IXUSR)
        try:
            with helpers.with_test_wd(base_dpath):
                with get_stdout_suppress_stderr():
                    rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        finally:
            base_dpath.chmod(stat.S_IRWXU)
        self.assertEqual(rc, 3)


    def test_build_error(self):
        '''
        Test provoking a build error.
        '''
        build_error = self.build_err_total
        variant = 'rel'
        (argv, _) = self.prep_test(
            ['-b', str(self.test_base_dpath)], build_error=build_error,
            variant=variant)
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.assertEqual(rc, 4)


    def test_no_opts_conf_file(self):
        '''
        Test for a Shinobi CONF configuration file using no options.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test([], example, use_json=False)
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)


    def test_no_opts_json_file(self):
        '''
        Test for a Shinobi JSON configuration file using no options.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test([], example, use_json=True)
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)


    def test_base_dpath(self):
        '''
        Test for the ``-b`` option.
        '''
        example = 'fibonacci'
        variant = 'rel'
        (argv, _) = self.prep_test(
            ['-b', str(self.test_base_dpath)], example, variant)
        with get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example, variant)
        self.assertEqual(rc, 0)


    def test_cfg_file(self):
        '''
        Test for the ``-c`` option.
        '''
        example = 'guess_nat'
        used_cfg_fpath = self.test_base_dpath / 'config.conf'
        (argv, cfg_fpath) = self.prep_test(
            ['-c', str(used_cfg_fpath)], example)
        shutil.move(cfg_fpath, used_cfg_fpath)
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)


    def test_rel_cfg_fpath_base_dpath(self):
        '''
        Test for `-c` option with an a relative file path, together with the
        `-b` option.
        '''
        example = 'fibonacci'
        variant = 'rel'
        args = ['-b', str(self.test_base_dpath), '-c', self.cfg_conf_fname]
        (argv, _) = self.prep_test(args, example, variant)
        with get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example, variant)
        self.assertEqual(rc, 0)


    def test_verb1(self):
        '''
        Test for a single ``-v`` option.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test(['-v'], example)
        with helpers.with_test_wd(), get_stdout_suppress_stderr() as cm:
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)
        self.assertMinLinesNr(cm.out, 3)


    def test_verb2(self):
        '''
        Test for a two ``-v`` options.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test(['-vv'], example)
        with helpers.with_test_wd(), get_stdout_suppress_stderr() as cm:
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)
        self.assertMinLinesNr(cm.out, 8)


    def test_verb3(self):
        '''
        Test for a three ``-v`` options.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test(['-vvv'], example)
        with helpers.with_test_wd(), get_stdout_suppress_stderr() as cm:
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)
        self.assertMinLinesNr(cm.out, 25)


    def test_debug(self):
        '''
        Test for a ``--debug`` option.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test(['--debug'], example)
        with helpers.with_test_wd(), get_stdout_suppress_stderr() as cm:
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)
        self.assertMinLinesNr(cm.out, 2200)


    def test_dry_run(self):
        '''
        Test for a ``-n`` option.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test(['-n'], example)
        with helpers.with_test_wd(), get_stdout_suppress_stderr() as cm:
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(
            example, exp_ex_out_fpaths=[], exp_noex_out_fpaths=None)
        self.assertEqual(rc, 0)
        self.assertMinLinesNr(cm.out, 1)


    def test_nr_tasks(self):
        '''
        Test for a ``-j`` option.
        '''
        example = 'guess_nat'
        (argv, _) = self.prep_test(['-j', '1'], example)
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(example)
        self.assertEqual(rc, 0)


    def test_keep_going(self):
        '''
        Test for a ``-k`` option.
        '''
        build_error = self.build_err_partial
        (argv, _) = self.prep_test(['-k'], build_error=build_error)
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(argv, add_run_args=self.silent_run_args)
        self.check_output_files(build_error=build_error)
        self.assertEqual(rc, 4)


    def test_oth_run_shinobi_fct(self):
        '''
        Test for an alternative Shinobi run function.
        '''
        my_fpath = self.test_base_dpath / 'foobar'

        def run_fake(cfg_fpath, cfg_params={}, base_dpath=None,
                     dry_run=False, nr_tasks=None, keep_going=False,
                     add_run_args={}):
            shutil.copy(cfg_fpath, my_fpath)
            return 0
        example = 'guess_nat'
        (argv, _) = self.prep_test([], example)
        with helpers.with_test_wd(), get_stdout_suppress_stderr():
            rc = cmdline.main(
                argv, add_run_args=self.silent_run_args,
                run_shinobi_fct=run_fake)
        self.check_output_files(
            exp_ex_out_fpaths=[my_fpath], exp_noex_out_fpaths=[])
        self.assertEqual(rc, 0)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
