#!/usr/bin/env python3
# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python script
#
# Author         : Dirk Ullrich
#
# Date           : 2021-08-17
#
# Description    : See documentation string below.
# ============================================================================

'''
Test helper script to generate all test data.
'''

import sys
import pathlib
this_dpath = pathlib.Path(__file__).parent
pkg_dpath = this_dpath.parent.parent
# Inserting before the standard Python module paths is important to always
# prefer the stuff from this very repository.
sys.path.insert(0, str(pkg_dpath))

import tests.test_config
import tests.test_ninja

# Functions
# =========


def main():
    # Test data related to :py:mod:`config` tests.
    print("Generating test data for 'config'...")
    tcc = tests.test_config.base_config_TC()
    tcc.setUp()
    fpaths = tcc.mk_test_data()
    print("Test data files for 'config' generated:")
    for f in fpaths:
        print(f"  {f}")

    # Test data related to :py:mod:`ninja` tests.
    print("Generating test data for 'ninja'...")
    tcn = tests.test_ninja.base_recipe_TC()
    tcn.setUp()
    fpaths = tcn.mk_test_data()
    print("Test data files for 'ninja' generated:")
    for f in fpaths:
        print(f"  {f}")


# Doing
# =====

if __name__ == '__main__':
    main()


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
