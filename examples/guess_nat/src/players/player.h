/*
 * Guess a natural number.
 * Player - header file.
 */

#ifndef PLAYER_H
#define PLAYER_H

/* Guess a natural number >= 0 and <= <max>, and return it.  Character
 * <hint_last> gives feedback for the last guess (if there was any):
 * + 'n': there was no last guess;
 * + 's': last guess was to small;
 * + 'l': last guess was to large. */
unsigned guess_next(unsigned max, char hint_last);

#endif
