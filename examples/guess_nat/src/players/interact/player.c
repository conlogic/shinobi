/*
 * Guess a natural number.
 * Interactive player - implementation file.
 */

#include <stdio.h>

#include "player.h"

unsigned guess_next(unsigned max, char hint_last) {
    unsigned next;
    scanf("%u", &next);

    return next;
}
