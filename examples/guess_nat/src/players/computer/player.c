/*
 * Guess a natural number.
 * Computer player (lion catch) - implementation file.
 */

#include <stdio.h>

#include "player.h"

/* Known upper and lower bound. */
static unsigned lower;
static unsigned upper;
/* Last guess. */
static unsigned last;

unsigned guess_next(unsigned max, char hint_last) {

    // Determine what bound is to be sharpened.
    switch (hint_last) {
    case 's':
         // Last guess was to small.
         lower = last;
         break;
    case 'l':
         // Last guess was to large.
         upper = last;
         break;
    case 'n':
         // No last guess.
         lower = 0;
         upper = max;
         break;
    }

    // Take next guess from the middle.
    unsigned next = (lower + upper) / 2;
    last = next;

    return next;
}
