/*
 * Guess a natural number.
 * Game framework - main implementation file.
 */

#include <stdlib.h>
#include <stdio.h>

#include "guess.h"
#include "player.h"

int main(int argc, char * argv[]) {
    init_game();

    int max = get_max(argc, argv);
    if (max == -1)
         return EXIT_FAILURE;

    unsigned secret = secret_nat(max);

    unsigned guess;
    char hint_last = 'n';
    do     {
        printf("Guess a number >= 0 and <= %u:\n", max);
        guess = guess_next(max, hint_last);
        if (guess < secret) {
            printf("Guessed number %u is to small.\n", guess);
            hint_last = 's';
        } else if (guess > secret) {
            printf("Guessed number %u is to large.\n", guess);
            hint_last = 'l';
        }
    } while (guess != secret);
    printf("Guessed number %u is correct.\n", guess);

    return EXIT_SUCCESS;
}

int get_max(int argc, char * argv[]) {
    unsigned max_limit = RAND_MAX;
    int max = 0;

    if (argc < 2) {
        printf("Missing argument for the maximal number.\n");
        max = -1;
    } else {
        max = atoi(argv[1]);
        if (max > max_limit) {
            printf("Maximal number must be <= %u", max_limit);
            max = -1;
        }
    }

    return max;
}
