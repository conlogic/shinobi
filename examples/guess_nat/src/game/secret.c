/*
 * Guess a natural number.
 * Game framework - implementation file to get a secret natural number.
 */

#include <stdlib.h>
#include <time.h>

#include "guess.h"

/* Initialize secret number generation. */
void init_game(void) {
    srand(time(NULL));
}

/* Return a secret natural number >= 0 and <= <max>. */
unsigned secret_nat(unsigned max) {
    return (rand() % (max + 1));
}
