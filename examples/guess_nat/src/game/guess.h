/*
 * Guess a natural number.
 * Game framework - main header file.
 */

#ifndef GUESS_H
#define GUESS_H

/* Validate the maximal number given by commandline, and return it.
 * A return value -1 signals a missing or an invalid argument. */
int get_max(int argc, char * argv[]);

/* Initialize secret number generation. */
void init_game(void);

/* Return a secret natural number >= 0 and <= <max>. */
unsigned secret_nat(unsigned max);

#endif
