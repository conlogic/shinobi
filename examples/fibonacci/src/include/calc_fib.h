/*
 * Print the n-th Fibonacci number.
 * Calculation - header file.
 */

/* Calculate the <n>-th Fibonacci number, and return it. */
unsigned long long fib(unsigned long long n);
