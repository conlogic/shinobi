/*
 * Print the n-th Fibonacci number.
 * Main program - implementation file.
 */

#include <stdio.h>
#include <stdlib.h>

#include "calc_fib.h"


int main(void) {
    printf("Please enter a number: ");
    unsigned long long n;
    scanf("%llu", &n);

    printf("The Fibonacci number for %llu is %llu.\n", n, fib(n));

    return EXIT_SUCCESS;
}
