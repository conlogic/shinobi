/*
 * Print the n-th Fibonacci number.
 * Calculation - implementation file.
 */

unsigned long long fib(unsigned long long n) {
    unsigned long long result;
    // We use a straightforward recursive calculation.
    if (n > 2)
         result = fib(n - 2) + fib(n - 1);
    else
         result = 1;

    return result;
}
