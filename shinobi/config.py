# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-08-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Handling Shinobi configurations and configuration files.
'''


import duconfig


# Classes
# =======


class Config(duconfig.Config):
    '''
    Store configuration used to generate Ninja recipes, with specific methods
    to ease this.
    '''
    def ninja_variables(self, raw=None):
        '''
        Get the Ninja recipe variables from the configuration.

        A not-``None`` switch `raw` forces whether the variable values are
        expanded (for a ``False`` value) or not (for a ``True`` value).  For a
        ``None`` value the defaults from the :py:class:`Config` object is
        used.

        :return: a dictionary whose keys are the variable names, and the whose
                 values are the variable values.
        '''
        # The section for Ninja variables is optional.
        if 'vars' in self.keys():
            variables = self.get('vars', raw)
        else:
            variables = {}

        return variables


    def ninja_rules(self, raw=None):
        '''
        Get the Ninja recipe rules from the configuration.

        A not-``None`` switch `raw` forces whether rules' content is
        expanded (for a ``False`` value) or not (for a ``True`` value).  For a
        ``None`` value the defaults from the :py:class:`Config` object is
        used.

        :return: a dictionary whose keys are the rule names, and the whose
                 values are the rule contents.

        :raise: a :py:exc:ConfigError` if the configuration defines no Ninja
                recipe rules.
        '''
        try:
            rules = self.get('rules', raw)
        except duconfig.ConfigError:
            raise duconfig.ConfigError(
                'No Ninja rules defined by the configuration.')

        return rules


    def ninja_goals(self, raw=None):
        '''
        Get the Ninja recipe goals ("builds" in Ninja's own terminology) from
        the configuration.

        A not-``None`` switch `raw` forces whether the goals parameters are
        expanded (for a ``False`` value) or not (for a ``True`` value).  For a
        ``None`` value the defaults from the :py:class:`Config` object is
        used.  Please note that value expansion also expands multi goals,
        i.e. goals with multiple output files, into their lists of single
        goals.

        :return: a dictionary whose keys are the goals names, and the whose
                 values are the goals parameters.

        :raise: a :py:exc:ConfigError` if the configuration defines no Ninja
                recipe goals.
        '''
        if raw is None:
            raw = self.raw

        try:
            raw_goals = self.get('goals', raw=True)
        except duconfig.ConfigError:
            raise duconfig.ConfigError(
                'No Ninja goals defined by the configuration.')

        if raw:
            all_goals = raw_goals
        else:
            # For not-`raw` we have to expand multi goals, too.
            all_goals = {}
            for (n, g) in raw_goals.items():
                if 'ins' in g.keys():
                    # Multi goal -> add each expanded goal with expanded
                    # values.
                    exp_g = self._expand_multi_goal(g)
                    goals_n_g = {
                        # Although the ``'out'`` value is already expanded,
                        # other goal values maybe not.
                        n + str(nr): self.expand_value(goal)
                        for (goal, nr) in zip(exp_g, range(len(exp_g)))
                    }
                else:
                    # No multi goal -> add the single goal with expanded
                    # values.
                    goals_n_g = {
                        n: self.expand_value(g)
                    }
                all_goals.update(goals_n_g)

        return all_goals


    def env_variables(self, raw=None):
        '''
        Get the environment variables and their values from the configuration.

        A not-``None`` switch `raw` forces whether the values are expanded
        (for a ``False`` value) or not (for a ``True`` value).  For a
        ``None`` value the defaults from the :py:class:`Config` object is
        used.

        :return: a dictionary whose keys are the environment variables, and
                 the whose values are their values.
        '''
        vars_vals = {}
        if 'env' in self.keys():
            env = self.get('env', raw)
            if 'vars' in env.keys():
                vars_vals = self.get('env.vars', raw)

        return vars_vals


    def _expand_multi_goal(self, goal):
        '''
        Expand raw multi goal `goal`.

        :return: list of expanded goals defined by `goal`.
        '''
        # Get expanded value for the goal's input paths.
        ins_expanded = self.expand_value(goal['ins'])
        # Iterate over the input paths of the goal, and create a single
        # goal for this input path.
        goals = []
        for i in sorted(ins_expanded):
            goal_i = goal.copy()
            # Specialize input path.
            del goal_i['ins']
            goal_i['in'] = i
            # Specialize output path template for the input path.
            out_i_tpl = goal['out']
            out_i_exp = out_i_tpl.replace('$in', i)
            goal_i['out'] = self.expand_value(out_i_exp)
            goals.append(goal_i)

        return goals


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
