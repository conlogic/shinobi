# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities to interact with Ninja.
'''


import pathlib
import os
import subprocess
import time

from . import utilities


# Functions and classes
# =====================


def write_ninja_recipe(recipe_fpath, variables, rules, goals,
                       header='Auto-generated Ninja recipe file.'):
    '''
    Write Ninja variables `variables`, Ninja rules `rules` and Ninja goals
    `goals` to a Ninja recipe file with path `recipe_fpath`.

    String `header` is used for the comment on the top of this file.

    :raise: a :py:exc:`NinjaRecipeError` if the :py:mod:`ninja_syntax` module
            is missing, or the recipe file cannot be written.
    '''
    # Try to load :py:mod:`ninja_syntax` used to write the pieces of a Ninja
    # recipe.
    try:
        import ninja_syntax
    except ModuleNotFoundError:
        raise NinjaRecipeError(
            "The `ninja_syntax` syntax module cannot be loaded.")

    # Try to write the recipe file.
    try:
        with open(recipe_fpath, 'wt') as recipe_fobj:
            writer = ninja_syntax.Writer(recipe_fobj)

            # Recipe file header.
            writer.comment(header)

            # Recipe variables, if wanted.
            if not variables == {}:
                utilities.log(
                    utilities.DETAIL_LOG_LEVEL,
                    'Using the following Ninja recipe variables:')
                for (var, val) in variables.items():
                    utilities.log(
                        utilities.DETAIL_LOG_LEVEL,
                        f"  {var} = {val}")
                    writer.newline()
                    writer.variable(var, val)

            # Recipe rules.
            utilities.log(
                utilities.DETAIL_LOG_LEVEL,
                'Using the following Ninja recipe rules:')
            for (name, cmd) in rules.items():
                utilities.log(
                    utilities.DETAIL_LOG_LEVEL,
                    f"  {name}:\n    {cmd}")
                writer.newline()
                writer.rule(name, cmd)

            # Recipe goals.
            utilities.log(
                utilities.DETAIL_LOG_LEVEL,
                'Using the following Ninja recipe goals:')
            for (_, goal) in goals.items():
                utilities.log(
                    utilities.DETAIL_LOG_LEVEL,
                    f"  {goal}")
                writer.newline()
                writer.build(goal['out'], goal['rule'], goal['in'])

            writer.close()
            utilities.log(
                2, f"->Ninja recipe file written to\n  {recipe_fpath}")

    except OSError:
        raise NinjaRecipeError(
            'Cannot write to the Ninja recipe file', loc=str(recipe_fpath))


def run_ninja(recipe_fpath, ninja_cmd='ninja', env_vars={},
              dry_run=False, nr_tasks=None, keep_going=False, base_dpath=None,
              add_run_args={}):
    '''
    Run Ninja using Ninja command `ninja_cmd` for the Ninja recipe file with
    path `recipe_fpath`.

    By dictionary `env_vars` additional environment variables are set: every
    key in `env_vars` is considered as environment variable that is set to its
    value from `env_vars`.

    A ``True`` switch `dry_run` causes a Ninja dry run instead of a real one.

    A not-``None`` number `nr_tasks` defines the number of parallel tasks that
    will be used for the Ninja run instead of letting Ninja automatically
    chose it.

    If `keep_going` switch is ``True`` Ninja does not stop after the first
    error.

    For a not-``None`` filepath `base_dpath` the directory with this path is
    used as base directory.  Otherwise the current working directory is used.

    The content of dictionary `add_run_args` is passed as keyword arguments to
    :py:meth:`subprocess.run` that runs Ninja.

    :return: the result returned by :py:meth:`subprocess.run`.

    :raise: a :py:exc:`NinjaCallError` if the Ninja run has failed.
    '''
    # Check that the Ninja command can be called.
    check_cmd = f"{ninja_cmd} --version"
    if not utilities.check_cmd(check_cmd):
        raise NinjaCallError(
            f"Calling Ninja by\n  {ninja_cmd}\n will fail.")

    # Put the full Ninja call together.
    # - Start with Ninja command.
    ninja_call = ninja_cmd
    # - Add option for working directory, if needed.
    if base_dpath is not None:
        base_dpath = pathlib.Path(base_dpath)
        utilities.log(
            2, f"->Use working directory at\n  {base_dpath}")
        ninja_call += f" -C {base_dpath}"
    else:
        utilities.log(
            2, '->Use current working directory.')
    # - Add option for verbosity, if needed.
    if utilities.is_active_loglevel(1):
        ninja_call += ' -v'
    # - Add option with Ninja recipe path used.
    used_recipe_fpath = pathlib.Path(recipe_fpath).absolute()
    ninja_call += f" -f {used_recipe_fpath}"
    # - Add option for number of parallel tasks, if wanted.
    if nr_tasks is not None:
        utilities.log(
            2, f"->Use {nr_tasks} parallel tasks.")
        ninja_call += f" -j {nr_tasks}"
    else:
        utilities.log(
            2, '->Number of parallel tasks will be automatically chosen.')
    # - Add option for keep-going, if wanted.
    if keep_going:
        utilities.log(
            2, '->Keep going during run.')
        ninja_call += ' -k 0'
    else:
        utilities.log(
            2, '->Run stops after the first error.')
    # - Add option for dry-run, if wanted.
    if dry_run:
        utilities.log(
            2, '->Do a dry run instead of a real one.')
        ninja_call += ' -n'

    # Set additional environment variables, if wanted.
    if not env_vars == {}:
        utilities.log(
            utilities.DETAIL_LOG_LEVEL,
            'Setting the following environment variables:')
        for (var, val) in env_vars.items():
            utilities.log(
                utilities.DETAIL_LOG_LEVEL, f"  {var}={val}")
    else:
        utilities.log(
            utilities.DETAIL_LOG_LEVEL,
            'Set no environment variables.')
    run_env = os.environ.copy()
    run_env.update(env_vars)

    # Run the Ninja call.
    utilities.log(
        1, f"===Ninja run start===\n  {ninja_call}")
    # - We are interested in the duration of the run.
    run_start_time = time.time()
    # - Run the real Ninja call.
    run_result = subprocess.run(
        ninja_call, shell=True, env=run_env, **add_run_args)
    # - Calculate the run's duration.
    run_end_time = time.time()
    run_duration = run_end_time - run_start_time
    # - Raise an error if the Ninja call has failed.
    run_rc = run_result.returncode
    if not run_rc == 0:
        raise NinjaCallError(
            f"Ninja call\n  {ninja_call}\n has failed with rc={run_rc}.",
            result=run_result)

    utilities.log(
        1, "===Ninja run took ({:.2f} s)===".format(run_duration))

    return run_result


class NinjaRecipeError(utilities.ShinobiError):
    '''
    Errors specific for generating Ninja recipes by Shinobi.
    '''
    pass


class NinjaCallError(utilities.ShinobiError):
    '''
    Errors specific for calling Ninja recipes by Shinobi.  It has an
    additional attribute to save Results from :py:func:`subprocess.run`.
    '''
    def __init__(self, msg, result=None, loc=None):
        super().__init__(msg, loc)
        self.result = result


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
