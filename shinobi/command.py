# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-08-19
#
# Description    : See documentation string below.
# ============================================================================

'''
Core API functions of Shinobi.
'''


import pathlib

from . import config
from . import functions
from . import ninja
from . import utilities


def run_shinobi(cfg_fpath, cfg_params={}, base_dpath=None, dry_run=False,
                nr_tasks=None, keep_going=False, add_run_args={}):
    '''
    Run Ninja to process configuration read from configuration file with path
    `cfg_fpath`.

    The content of dictionary `cfg_params` is directly passed as keyword
    arguments to initialize the :py:class:`config.Config` object holding the
    configuration.

    For a not-``None`` filepath `base_dpath` the directory with this path is
    used as base directory.  Otherwise the current working directory is used.

    A ``True`` switch `dry_run` causes a Ninja dry run instead of a real one.

    A not-``None`` number `nr_tasks` defines the number of parallel tasks that
    will be used for the Ninja run instead of letting Ninja automatically
    chose it.

    If `keep_going` switch is ``True`` Ninja does not stop after the first
    error.

    The content of dictionary `add_run_args` is passed as keyword arguments to
    :py:meth:`subprocess.run` that runs Ninja.

    :return: the result returned by :py:meth:`subprocess.run`.

    :raise: a :py:exc:`duconfig.Config` if processing of the configuration has
            failed.
    :raise: a :py:exc:`NinjaRecipeError` if Ninja recipe creation has failed.
    :raise: a :py:exc:`NinjaCallError` if the Ninja run has failed.
    '''
    # Read the configuration file.
    cfg = read_config_file(cfg_fpath, cfg_params)

    # Create the recipe file, set additional environment variables and call
    # ninja for the created recipe file.
    run_results = run_shinobi_for_config(
        cfg, base_dpath, dry_run, nr_tasks, keep_going, add_run_args)

    return run_results


def read_config_file(cfg_fpath, cfg_params={}):
    '''
    Read configuration file with path `cfg_fpath`.

    The content of dictionary `cfg_params` is directly passed as keyword
    arguments to initialize the :py:class:`config.Config` object holding the
    configuration.

    :return: the :py:class:`config.Config` object holding the configuration.

    :raise: a :py:exc:`duconfig.ConfigError` if goals' processing fails.
    '''
    # If no functions available for interpolation are given yet, we add the
    # default ones.
    if 'fcts' not in cfg_params.keys():
        cfg_args = cfg_params.copy()
        cfg_args['fcts'] = functions.CFG_FILE_FUNCTIONS
    else:
        cfg_args = cfg_params

    # Create an appropriate configuration object.
    cfg = config.Config(raw=False, **cfg_args)

    # Read the configuration file.
    cfg.read_file(cfg_fpath)

    return cfg


def run_shinobi_for_config(cfg, base_dpath=None, dry_run=False, nr_tasks=None,
                           keep_going=False, add_run_args={}):
    '''
    Run Ninja to process configuration given by :py:class:`config.Config`
    object `cfg`.

    For a not-``None`` filepath `base_dpath` the directory with this path is
    used as base directory.  Otherwise the current working directory is used.

    A ``True`` switch `dry_run` causes a Ninja dry run instead of a real one.

    A not-``None`` number `nr_tasks` defines the number of parallel tasks that
    will be used for the Ninja run instead of letting Ninja automatically
    chose it.

    If `keep_going` switch is ``True`` Ninja does not stop after the first
    error.

    The content of dictionary `add_run_args` is passed as keyword arguments to
    :py:meth:`subprocess.run` that runs Ninja.

    :return: the result returned by :py:meth:`subprocess.run`.

    :raise: a :py:exc:`duconfig.Config` if processing of `cfg` has failed.
    :raise: a :py:exc:`NinjaRecipeError` if Ninja recipe creation has failed.
    :raise: a :py:exc:`NinjaCallError` if the Ninja run has failed.
    '''
    # To handle paths in the configuration properly we have to enter the base
    # directory first.
    if base_dpath is not None:
        base_dpath = pathlib.Path(base_dpath)
        if not base_dpath.is_dir():
            raise ninja.NinjaCallError(
                'There is no base directory', loc=base_dpath)
    with utilities.with_wd(base_dpath):
        # Write the Ninja recipe.
        # - Ninja variables.
        variables = cfg.ninja_variables(raw=False)
        # - Ninja rules.
        rules = cfg.ninja_rules(raw=False)
        # - Ninja goals.
        goals = cfg.ninja_goals(raw=False)
        # - Name for the Ninja recipe file.
        recipe_fpath = pathlib.Path('build.ninja').absolute()
        ninja.write_ninja_recipe(recipe_fpath, variables, rules, goals)
        # Set environment variables to be set.
        env_vars = cfg.env_variables(raw=False)

    # Run Ninja.
    # - Determine the Ninja command to be used.
    if 'env' in cfg.keys() and 'ninja_cmd' in cfg['env']:
        ninja_cmd = cfg.get('env.ninja_cmd', raw=False)
    else:
        ninja_cmd = 'ninja'
    # - Run this Ninja command.
    run_result = ninja.run_ninja(
        recipe_fpath, ninja_cmd, env_vars,
        dry_run, nr_tasks, keep_going, base_dpath, add_run_args)

    return run_result


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
