# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-08-18
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities used throughout the whole package.
'''


import pathlib
import os
import subprocess

import dubasiclog


# Parameters
# ==========

# Maximal interpolation depth.
MAX_INTERPOLATION_DEPTH = 10

# Log level for details.
DETAIL_LOG_LEVEL = 3


# Functions and classes
# =====================


# For uniform logging we use the module-global logging facilities from package
# ``dubasiclog``.
set_max_loglevel = dubasiclog.set_max_loglevel
enable_debug_log = dubasiclog.enable_debug_log
disable_debug_log = dubasiclog.disable_debug_log
is_active_loglevel = dubasiclog.is_active_loglevel
log = dubasiclog.log
debug_log = dubasiclog.debug_log


def has_file_ext(fpath, fext):
    '''
    :return: ``True`` if filepath `fpath` has file extension `fext`.
    '''
    fpath = pathlib.Path(fpath)

    return fpath.suffix == (os.extsep + fext)


def find_in_dir(pred, dpath):
    '''
    :return: list of all paths ``f`` for file below directory with path
             `dpath` such that for predicate `pred` holds for ``f``, i.e. the
             value of `pred` for ``f`` is ``True``.
    '''
    fpaths_for_pred = []
    for (d, _, fnames) in os.walk(dpath):
        d = pathlib.Path(d)
        fpaths_d = [d / n for n in fnames]
        fpaths_d_for_pred = [f for f in fpaths_d if pred(f)]
        fpaths_for_pred.extend(fpaths_d_for_pred)

    return fpaths_for_pred


def check_cmd(cmd):
    '''
    :return: ``True`` iff calling command `cmd` was successful, and ``False``
             otherwise.
    '''
    check_res = subprocess.run(
        cmd, shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    check_ok = check_res.returncode == 0

    return check_ok


class with_wd(object):
    '''
    Context manager to use an alternative working directory.
    '''
    def __init__(self, work_dpath=None):
        '''
        Initialize a context to use working directory with path `work_dpath`.

        If `work_dpath` is ``None``, the working directory is not changed.
        '''
        self.work_dpath = work_dpath


    def __enter__(self):
        '''
        Enter a context to use an alternative working directory.
        '''
        if self.work_dpath is not None:
            self._orig_work_dpath = pathlib.Path.cwd()
            os.chdir(self.work_dpath)


    def __exit__(self, ex_type, ex_obj, traceback):
        '''
        Leave a context to use an alternative working directory, where for
        not-``None`` type `exc_type` is the type of raised exception, a
        not-``None`` `exc_obj` is the exception raised, and a not-``None``
        `traceback` is the associated trace back.
        '''
        if self.work_dpath is not None:
            os.chdir(self._orig_work_dpath)


class ShinobiError(Exception):
    '''
    Errors specific for the ``shinobi`` package.
    '''
    def __init__(self, msg, loc=None):
        '''
        Initialize a :py:class:`ShinobiError` with corresponding message `msg`,
        and an optional location information `loc`.
        '''
        self.msg = msg
        self.loc = loc


    def __str__(self):
        '''
        :return: printable string representation of the exception.
        '''
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` or not we ensure first that there
        # is no ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg

        # When a location is given, we start a new line for it, but do not
        # want a ``'.'`` at the end.  Otherwise we ensure one.
        if self.loc is not None:
            ex_str += f" at\n  {self.loc}"
        else:
            ex_str += '.'

        return ex_str


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
