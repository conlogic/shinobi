# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2021-09-19
#
# Description    : See documentation string below.
# ============================================================================



'''
Helper functions that can be called in configuration files.
'''


import pathlib
import os

import shinobi.utilities as utilities


# Functions
# =========


def file_base(fpath):
    '''
    :return: file base of filepath `fpath`, i.e. `fpath` with its directory
             part and extension stripped.

    Examples:

    >>> file_base('foo/bar.baz')
    'bar'

    >>> file_base('bar.baz')
    'bar'

    >>> file_base('foo/bar')
    'bar'
    '''
    fpath = pathlib.Path(fpath)

    return fpath.stem


def files_bases(fpaths):
    '''
    :return: list of file bases for the filepaths in list `fpaths`.

    Example:

    >>> files_bases(['foo/bar1.baz', 'bar2.baz', 'foo/bar3'])
    ['bar1', 'bar2', 'bar3']
    '''
    fbases = [file_base(f) for f in fpaths]

    return fbases


def file_dir(fpath):
    '''
    :return: directory part of filepath `fpath` as string.

    Examples:

    >>> file_dir('foo/bar.baz')
    'foo'

    >>> file_dir('bar.baz')
    '.'

    >>> file_dir('foo/bar')
    'foo'
    '''
    fpath = pathlib.Path(fpath)

    return str(fpath.parent)


def files_dirs(fpaths):
    '''
    :return: list of directory parts as strings for the filepaths in list
             `fpaths`.

    Example:

    >>> files_dirs(['foo1/bar.baz', 'bar.baz', 'foo2/bar'])
    ['foo1', '.', 'foo2']
    '''
    dpaths = [file_dir(f) for f in fpaths]

    return dpaths


def file_noext(fpath):
    '''
    :return: filepath `fpath` with file extension stripped, if there is any.

    Examples:

    >>> file_noext('foo')
    'foo'

    >>> file_noext('foo.bar')
    'foo'

    >>> file_noext('foo/bar')
    'foo/bar'

    >>> file_noext('foo/bar.baz')
    'foo/bar'
    '''
    fpath = pathlib.Path(fpath)
    fpath_noext = fpath.with_suffix('')

    return str(fpath_noext)


def files_noext(fpaths):
    '''
    :return: list of filepath `fpaths` with file extension stripped of each,
             if there is any.

    Examples:

    >>> files_noext([])
    []

    >>> files_noext(['foo1', 'foo2.bar2', 'foo3/bar3', 'foo4/bar4.baz'])
    ['foo1', 'foo2', 'foo3/bar3', 'foo4/bar4']
    '''
    fpaths_noext = [file_noext(f) for f in fpaths]

    return fpaths_noext


def add_file_ext(fpath, fext):
    '''
    :return: filepath `fpath` as string with file extension `fext` added.

    Examples:

    >>> add_file_ext('foo/bar', 'baz')
    'foo/bar.baz'

    >>> add_file_ext('foo.bar', 'baz')
    'foo.bar.baz'
    '''
    fpath = pathlib.Path(fpath)
    old_fext = fpath.suffix
    new_fext = old_fext + os.extsep + fext

    return str(fpath.with_suffix(new_fext))


def add_files_ext(fpaths, fext):
    '''
    :return: list of filepaths `fpaths` as strings with file extension `fext`
             added to each of them.

    Example:

    >>> add_files_ext(['foo1/bar1', 'foo2.bar2'], 'baz')
    ['foo1/bar1.baz', 'foo2.bar2.baz']
    '''
    fpaths_with_ext = [add_file_ext(f, fext) for f in fpaths]

    return fpaths_with_ext


def add_file_dir(dpath, fpath):
    '''
    :return: filepath `fpath` as string with (additional) dictionary path
             `dpath` added.

    Examples:

    >>> add_file_dir('/foo', 'bar')
    '/foo/bar'

    >>> add_file_dir('foo', 'bar/baz')
    'foo/bar/baz'

    >>> add_file_dir('/foo', '/bar/baz')
    '/bar/baz'

    >>> add_file_dir('foo/bar1', 'bar2/baz')
    'foo/bar1/bar2/baz'
    '''
    dpath = pathlib.Path(dpath)

    return str(dpath / fpath)


def add_files_dir(dpath, fpaths):
    '''
    :return: list of filepaths `fpaths` as strings with (additional) directory
             path `dpath` added to each of them.

    Example:

    >>> add_files_dir('foo', ['bar', 'bar/baz', '/bar/baz'])
    ['foo/bar', 'foo/bar/baz', '/bar/baz']
    '''
    fpaths_with_dir = [add_file_dir(dpath, f) for f in fpaths]

    return fpaths_with_dir


def join_paths(paths):
    '''
    :return: the paths from list `paths` joined to a string using the path
             separator.

    Examples:

    >>> join_paths(['foo/bar'])
    'foo/bar'

    >>> join_paths(['foo', '/bar', 'bar/baz'])
    'foo:/bar:bar/baz'
    '''
    paths_str = os.pathsep.join(paths)

    return paths_str


def join_lists(*lists):
    '''
    :return: the list resulting from concatenating all lists in argument list
             `lists`.

    Examples:

    >>> join_lists()
    []

    >>> join_lists(['foo', 'bar'])
    ['foo', 'bar']

    >>> join_lists(['foo', 'bar'], ['bar'], ['bar', 'baz'])
    ['foo', 'bar', 'bar', 'bar', 'baz']
    '''
    joined_list = []
    for l in lists:
        joined_list.extend(l)

    return joined_list


def diff_lists(*lists):
    '''
    :return: the list of all elements for the 1st list in argument list
             `lists` that are not element of any other list in `lists`, or
             ``[]`` if there are no arguments.

    Examples:

    >>> diff_lists()
    []

    >>> diff_lists(['foo', 'bar'])
    ['foo', 'bar']

    >>> diff_lists(['foo', 'bar', 'baz'], ['bar', 'boo'], ['bar', 'baz'])
    ['foo']
    '''
    if len(lists) == 0:
        diffed_list = []
    else:
        not_list = join_lists(*lists[1:])
        diffed_list = [x for x in lists[0] if x not in not_list]

    return diffed_list


def common_lists(*lists):
    '''
    :return: the list of all elements from the 1st list in argument list
             `lists` that are also element of every other list in `lists`, or
             ``None`` if there are no arguments.

    Examples:
    >>> common_lists()

    >>> common_lists(['foo', 'bar'])
    ['foo', 'bar']

    >>> common_lists(['foo', 'bar', 'baz'], ['baz', 'foo'])
    ['foo', 'baz']

    >>> common_lists(['foo', 'bar'], ['foo', 'baz'], ['bar', 'baz'])
    []
    '''
    if len(lists) == 0:
        com_list = None
    else:
        com_list = []
        for x in lists[0]:
            x_is_common = True
            for l in lists[1:]:
                if x not in l:
                    x_is_common = False
                    break
            if x_is_common:
                com_list.append(x)

    return com_list


def unique_list(lst):
    '''
    :return: a list with the elements of list `lst` but all but the first
             occurrence of every element removed.

    Examples:
    >>> unique_list([])
    []

    >>> unique_list(['foo', 'bar', 'baz'])
    ['foo', 'bar', 'baz']

    >>> unique_list(['foo', 'bar', 'foo'])
    ['foo', 'bar']
    '''
    uniq_list = []
    for e in lst:
        if e not in uniq_list:
            uniq_list.append(e)

    return uniq_list


def find_ext_in_dir(fext, dpath):
    '''
    :return: sorted list of all paths ``f`` as strings for files below
             directory with path `dpath` such that ``f`` has file extension
             `fext`.

    Example:

    >>> d = pathlib.Path('/tmp', 'foo')
    >>> d.mkdir(exist_ok=True)
    >>> (d / 'baz1.txt').touch()
    >>> (d / 'bar').mkdir(exist_ok=True)
    >>> (d / 'bar' / 'baz2.txt').touch()
    >>> find_ext_in_dir('txt', d)
    ['/tmp/foo/bar/baz2.txt', '/tmp/foo/baz1.txt']
    '''
    def pred_fext(f):
        return utilities.has_file_ext(f, fext)

    found_paths = [str(f) for f in utilities.find_in_dir(pred_fext, dpath)]
    found_paths.sort()

    return found_paths


def find_ext_in_dirs(fext, dpaths):
    '''
    :return: sorted list of all paths ``f`` as strings for files below any of
             the directories with a path in list `dpaths` such that ``f`` has
             file extension `fext`.

    Example:

    >>> d1 = pathlib.Path('/tmp', 'foo1')
    >>> d1.mkdir(exist_ok=True)
    >>> d2 = pathlib.Path('/tmp', 'foo2')
    >>> d2.mkdir(exist_ok=True)
    >>> (d1 / 'baz11.txt').touch()
    >>> (d1 / 'bar').mkdir(exist_ok=True)
    >>> (d1 / 'bar' / 'baz12.txt').touch()
    >>> (d2 / 'baz2.txt').touch()
    >>> find_ext_in_dirs('txt', [d1, d2])
    ['/tmp/foo1/bar/baz12.txt', '/tmp/foo1/baz11.txt', '/tmp/foo2/baz2.txt']
    '''
    found_paths_lists = [find_ext_in_dir(fext, d) for d in dpaths]

    found_paths = []
    for fs in found_paths_lists:
        found_paths.extend(fs)
    found_paths.sort()

    return found_paths


# List of standard functions available for interpolation in configuration
# files.
CFG_FILE_FUNCTIONS = [
    file_base,
    files_bases,
    file_dir,
    files_dirs,
    file_noext,
    files_noext,
    add_file_ext,
    add_files_ext,
    add_file_dir,
    add_files_dir,
    join_paths,
    join_lists,
    diff_lists,
    common_lists,
    unique_list,
    find_ext_in_dir,
    find_ext_in_dirs
]


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
