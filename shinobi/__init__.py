# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-08-09
#
# Description    : See documentation string below.
#
# ============================================================================

'''
Init module for package ``shinobi``.
'''


# Version of the very package.
version = '4.0.3'


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
