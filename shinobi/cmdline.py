# ============================================================================
# Title          : Simple build system based on Ninja - aka Shinobi
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-08-09
#
# Description    : See documentation string below.
# ============================================================================

'''
Commandline interface for package ``shinobi``.
'''


import pathlib
import os
import sys

try:
    from duargparse import DUArgumentParser as _ArgumentParser
except ModuleNotFoundError:
    from argparse import ArgumentParser as _ArgumentParser
import duconfig

from . import utilities
from . import ninja
from . import command
from . import version


# Functions and classes
# =====================


def main(argv=sys.argv, version=version,
         tool='Shinobi', run_shinobi_fct=command.run_shinobi,
         add_run_args={}):
    '''
    Main function using the commandline arguments list `argv`.

    String `version` is used as program version, and string `tool` is the name
    of this very tool.

    Function `run_shinobi_fct` processes the goals defined in the used
    configuration file.  `run_shinobi_fct` is expected to have one positional
    argument for the path of the configuration file used, and at least the
    following keys arguments:

    + `cfg_params`: a dictionary directly used as keyword arguments to
      initialize the  :py:class:`config.Config` object holding the
      configuration.

    + `base_dpath`: if not ``None`` the directory with this path is used as
      base directory instead of the current working directory.

    + `dry_run`: a switch that is ``True`` iff a dry run is used for the
      goals' processing;

    + `nr_tasks`: a not-``None`` number for a fixed number of parallel tasks
      when Ninja is running;

    + `keep_going`: a switch that is ``True`` iff Shinobi keeps Ninja running
      even after errors during the Ninja call have occurred.

    + `add_run_args`: a dictionary of additional keyword arguments used for
      running Ninja.

    The content of dictionary `add_run_args` is passed as keyword arguments to
    :py:meth:`subprocess.run` that runs Ninja.

    :return: 0 if there were no errors.
    '''
    try:
        # Parse the commandline.
        cmdline_parser = mk_cmdline_parser(argv[0], version, tool)
        parsed_args = cmdline_parser.parse_args(argv[1:])

        # Collect parameters needed for the command.
        base_dpath = parsed_args.base_dir
        cfg_fbase_default = tool.lower()
        cfg_fpath = calc_config_fpath(
            parsed_args.config_file, cfg_fbase_default, base_dpath)
        dry_run = parsed_args.dry_run
        nr_tasks = parsed_args.nr_tasks
        keep_going = parsed_args.keep_going

        # Set up logging.
        utilities.set_max_loglevel(parsed_args.verbose)
        if parsed_args.debug:
            utilities.enable_debug_log()

        # Run the command.
        run_shinobi_fct(
            cfg_fpath, base_dpath=base_dpath,
            dry_run=dry_run, nr_tasks=nr_tasks, keep_going=keep_going,
            add_run_args=add_run_args)

        rc = 0
    except (duconfig.ConfigError, utilities.ShinobiError) as ex:
        rc = handle_error(ex)

    return rc


def mk_cmdline_parser(prog, vers, tool):
    '''
    Create the commandline parser for this very tool with name `tool` and
    version `vers` called as `prog`.

    :return: the created commandline parser.
    '''
    # Program name.
    prog_name = pathlib.Path(prog).name

    # Create commandline parser.
    cmdline_parser = ArgumentParser(
        prog=prog_name,
        description=f"""
        Create from the {tool} configuration file given by its path a list of
        corresponding Ninja build recipes, and run Ninja for them.
        """)
    # Add option for version.
    cmdline_parser.add_argument(
        '-V', '--version',
        action='version', version=f"%(prog)s {vers}")

    # Add option for the path of the base directory.
    cmdline_parser.add_argument(
        '-b', '--base-dir', help=f"""
        Use base directory with path <base_dir> for {tool} instead of the
        current working directory.""",
        action='store', default=None)

    # Add option for the path of a Shinobi configuration file.
    cmdline_parser.add_argument(
        '-c', '--config-file', help=f"""
        Path of the {tool} configuration file to be used.  If not given the
        base directory is searched for a {tool} configuration file.""",
        action='store', default=None)

    # Add option to increase maximal log level.
    cmdline_parser.add_argument(
        '-v', '--verbose', help='''
        Increase verbosity level of log messages by one.  This option can
        given multiple times for multiple increase.''',
        action='count', default=0)

    # Add option for debugging log messages.
    cmdline_parser.add_argument(
        '-d', '--debug', help='''
        Enable debugging by activating all log messages.''',
        action='store_true', default=False)

    # Add option for dry-run.
    cmdline_parser.add_argument(
        '-n', '--dry-run', help='''
        The Ninja builds will not be done but only simulated.''',
        action='store_true', default=False)

    # Add option for number of parallel builds.
    cmdline_parser.add_argument(
        '-j', '--nr-tasks', help='''
        Number of parallel Ninja tasks instead of letting Ninja chose this
        number automatically.''',
        action='store', default=None)

    # Add option to ignore errors.
    cmdline_parser.add_argument(
        '-k', '--keep-going', help=f"""
        {tool} keeps Ninja running even after errors during the Ninja call have
        occurred.""",
        action='store_true', default=False)

    return cmdline_parser


def calc_config_fpath(fpath_maybe, fbase_default, base_dpath):
    '''
    Calculate the path for the Shinobi configuration file: if `fpath_maybe` is
    not ``None``, this path is taken made absolute relative to base directory
    given by path `base_dpath`.  Otherwise in the latter directory a file with
    base `fbase_default` and file extension ``conf`` or ``json`` (in this
    order) is searched.  Note that for a ``None`` value of `base_dpath` the
    current working directory is used.

    :return: the calculated Shinobi configuration filepath, if there is one.

    :raise: a :py:exc:`CommandlineError` if no Shinobi configuration filepath
            could be calculated.
    '''
    if base_dpath is None:
        base_dpath = pathlib.Path.cwd()
    else:
        base_dpath = pathlib.Path(base_dpath)

    if fpath_maybe is None:
        cfg_fnames_maybe = [
            pathlib.Path(fbase_default).with_suffix(os.extsep + e)
            for e in ['conf', 'json']
        ]
        cfg_fpath = None
        for n in cfg_fnames_maybe:
            fpath_n = base_dpath / n
            if fpath_n.is_file():
                cfg_fpath = fpath_n
                break
        if cfg_fpath is None:
            msg = 'There is neither a configuration file with name\n'
            for n in cfg_fnames_maybe:
                msg += f"- '{n}'\n"
            msg += f"in the base directory at\n  {base_dpath}\n"
            msg += "Please specify one explicitly using option '-c'."
            raise CommandlineError(msg)
    else:
        fpath_maybe = pathlib.Path(fpath_maybe)
        if fpath_maybe.is_absolute():
            cfg_fpath = fpath_maybe
        else:
            cfg_fpath = base_dpath / fpath_maybe

    return cfg_fpath


def handle_error(exception):
    '''
    Handle the error signaled by exception `exception`: an appropriate message
    for `exception` is printed to STDERR, and a return code associated with
    `exception` is calculated.

    :return: the return code calculated for `exception`.
    '''
    if isinstance(exception, CommandlineError):
        msg = f"Error during processing the commandline:\n{exception}"
        rc = 1
    elif isinstance(exception, duconfig.ConfigError):
        msg = f"Error during processing the configuration:\n{exception}"
        rc = 2
    elif isinstance(exception, ninja.NinjaRecipeError):
        msg = f"Error during creating the Ninja recipe:\n{exception}"
        rc = 3
    elif isinstance(exception, ninja.NinjaCallError):
        msg = f"Error during running Ninja\n{exception}"
        rc = 4
    else:
        msg = f"Unknown error:\n{repr(exception)}"
        rc = 5

    print(msg, file=sys.stderr)

    return rc


class ArgumentParser(_ArgumentParser):
    '''
    Variant for :py:class:`ArgumentParser` that raises an
    :py:exc:`ShinobiError` when failing.
    '''
    def error(self, message):
        raise CommandlineError(f"{message}\n{self.format_help()}")


class CommandlineError(utilities.ShinobiError):
    '''
    Errors specific for handling the commandline by Shinobi.
    '''
    pass


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Shinobi
