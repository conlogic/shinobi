..
 =============================================================================
 Title          : Simple build system based on Ninja - aka Shinobi

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2021-10-18

 Description    : Read me file for the package.
 =============================================================================


===============================================
``shinobi``: simple build system based on Ninja
===============================================

This package provides *Shinobi* [#origin]_, a simple generic build system
using the `Ninja`_ build system as back-end.

Ninja itself is optimized for performance.  A key factor for achieving this
goal is to keep the Ninja recipe files that configure the builds done by Ninja
simple.  Thus Ninja recipe files have variables, but there are only concrete
rules and goals [#goal]_.  Accordingly, *Shinobi* tries to extend the rather
elementary features of Ninja to configure builds: *Shinobi* allows more
powerful configuration files.  From such an own configuration file, *Shinobi*
generates a Ninja recipe file, followed by calling Ninja to process this
recipe file.

Actually, *Shinobi* enhances Ninja's functionality by the following features:

1. Configuration files have a flexible and powerful syntax:

  a. Both JSON and (an enhanced form of) CONF format is allowed.

  b. One can refer within configuration parameter values to the value of
     other parameter values (similar to global variables in Ninja recipe
     files).

  c. Within configuration values functions from a given list can be called.

2. Setting the environment from within the configuration file.

3. The Ninja command call to be used can be configured.

4. Support for an additional type of build goals, namely multi goals -
   bundling multiple goals with the same structure.

By the way, you can find a copy of the documentation for this package
`online`_.


.. [#origin] "Shinobi" is a traditional term to name a Ninja.

.. [#goal] Goals are called "builds" in Ninja terminology.

.. _Ninja: https://ninja-build.org
.. _online: .. _Ninja: https://ninja-build.org


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Shinobi
